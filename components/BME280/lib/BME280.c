#include "BME280.h"
#include "BME280_trim.h"
#include "i2c_easycomms.h"
#include "sdkconfig.h"
#define LOG_LOCAL_LEVEL CONFIG_BME_LOGLVL
#include "esp_log.h"

#define READY_MASK 0b00001001

#define MEASURE_SETTINGS 0b00100100
#define TAKE_MEASURE 0b00000010

static const char *TAG = "BME";

// Trimming parameters, read from chip
static uint16_t temp_t1 = 0;
static int16_t temp_trim[2] = {0,0};

static uint16_t pres_p1 = 0;
static int16_t pres_trim[8] = {0,0,0,0,0,0,0,0};

static uint8_t humi_h1 = 0;
static uint8_t humi_h3 = 0;
static int8_t humi_h6 = 0;
static int16_t humi_trim[3] = {0,0,0};  // H2, H4 & H5

struct bme280_calib_data trim_params;

static bool init_success = false;

static void check_ready(uint8_t deviceadr)                         // check whether sensor is busy copying values to registers
{
    const char *TAG = "WAIT";
    uint8_t status = 1;
    i2c_cmd_handle_t cmd;

    ESP_ERROR_CHECK( init_i2c() ); // initialize I2C connection (if not initialized previously)
    while (status != 0)
    {
        cmd = i2c_cmd_link_create();
    
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (deviceadr << 1) | I2C_MASTER_WRITE, WRITE_ACK);     // indicate WRITE to BME280 for register
        i2c_master_write_byte(cmd, 0xF3, WRITE_ACK);                                    // select register 0xF3 for status bits
        i2c_master_start(cmd);                                                          // start, so sensor knows the following address is not data
        i2c_master_write_byte(cmd, (deviceadr << 1) | I2C_MASTER_READ, WRITE_ACK);      // indicate READ to BME280
        i2c_master_read_byte(cmd, &status, I2C_MASTER_NACK);                                  // read status register into variable status
        i2c_master_stop(cmd);
        ESP_ERROR_CHECK(i2c_exec_cmd(&cmd) ); // do it
        i2c_cmd_link_delete(cmd);

        ESP_LOGV(TAG, "Status: %#04x", status );
        if ( ( status & READY_MASK ) )                                                  // if bit 0 or 3 are 1, it means the device isn't ready.
        {                                                                               // So if status & ready bitmask are nonzero, we should wait and retry.
            status &= READY_MASK;
            vTaskDelay (1 / portTICK_RATE_MS);
        }
        else return;
    }
}


#if LOG_LOCAL_LEVEL == ESP_LOG_VERBOSE
//Function for testing readability of BME280 registers; no longer needed, since code flaw has been detected
void test_read(uint8_t address)
{
    const char *TAG = "TEST";
    i2c_cmd_handle_t cmd;
    uint8_t registeradr = 0x88;
    uint8_t data;

    // First, let's read the reset register (always 0)
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);       // indicate WRITE to BME280 for register
    i2c_master_write_byte(cmd, 0xE0, WRITE_ACK);                                    // select register 0xF3 for reset bits
    i2c_master_start(cmd);                                                          // start, so sensor knows the following address is not data
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_READ, WRITE_ACK);        // indicate READ to BME280
    i2c_master_read_byte(cmd, &data, I2C_MASTER_NACK);                                    // read reset register into variable data
    i2c_master_stop(cmd);
    ESP_ERROR_CHECK(i2c_exec_cmd(&cmd) ); // do it
    i2c_cmd_link_delete(cmd);

    ESP_LOGV(TAG, "Register 0xE0 (reset): %#04x", data);

    while (registeradr < 0xFF)
    {
        cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);       // indicate WRITE to BME280 for register
        i2c_master_write_byte(cmd, registeradr, WRITE_ACK);                             // select register
        i2c_master_start(cmd);                                                          // start, so sensor knows the following address is not data
        i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_READ, WRITE_ACK);        // indicate READ to BME280
        i2c_master_read_byte(cmd, &data, I2C_MASTER_NACK);                                    // read register into variable data
        i2c_master_stop(cmd);
        ESP_ERROR_CHECK(i2c_exec_cmd(&cmd) ); // do it
        i2c_cmd_link_delete(cmd);

        ESP_LOGV(TAG, "Register %#04x: %#04x", registeradr, data);
        registeradr++;
    }
    vTaskDelay(10000 / portTICK_PERIOD_MS);
}

#endif // EXTREME_DEBUG, printing all registers

esp_err_t init_BME(uint8_t address)
{
    if (init_success) return ESP_OK;
    esp_err_t err = ESP_OK;
    ESP_ERROR_CHECK( init_i2c() ); // initialize I2C connection (if not initialized previously)
    uint8_t chipid = 0;
    uint8_t hum_buffer[4] = {0,0,0,0};
    i2c_cmd_handle_t cmd;

#if LOG_LOCAL_LEVEL == ESP_LOG_VERBOSE
    test_read(address);
#endif // EXTREME_DEBUG, execute read of all registers
    check_ready(address);
    
    //First, soft reset the sensor
    cmd = i2c_cmd_link_create();   // send measurement settings to BME sensor
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // indicate WRITE to BME280
    write_master(&cmd, 0xE0, 0xB6);                                             // write reset value to reset register
    i2c_master_stop(cmd);
    err = i2c_exec_cmd(&cmd);
    i2c_cmd_link_delete(cmd);
    ESP_ERROR_CHECK(err);
    ESP_LOGD(TAG, "Soft reset BME280.");
    
    check_ready(address);
    cmd = i2c_cmd_link_create();   // send measurement settings to BME sensor
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // indicate WRITE to BME280
    write_master(&cmd, 0xF2, 1);                 // make BME280 humidity only once when asked
    write_master(&cmd, 0xF4, MEASURE_SETTINGS);  // make BME280 measure temp and pressure only once when asked; first 3 and second 3 bits are temperature and pressure oversampling, last 2 bits are mode (still sleep)
    i2c_master_stop(cmd);
    err = i2c_exec_cmd(&cmd);
    i2c_cmd_link_delete(cmd);
    ESP_ERROR_CHECK(err);
    ESP_LOGD(TAG, "Transmitted measurement settings to BME280.");

    check_ready(address);
    read_word(0x88, &temp_t1, address, 0x89);                                      // Read temp_t1
    read_word_s(0x8A, temp_trim, address, 0x8D);                                     // Read temp_t2 and t3
    read_word(0x8E, &pres_p1, address, 0x8F);                                      // Read pres_p1
    read_word_s(0x90, pres_trim, address, 0x9F);                                     // Read pres_p2 to p9
    read_master(0xA1, &humi_h1, address, 0xA1);                                    // Read humi_h1 (uint8)
    read_master(0xE3, &humi_h3, address, 0xE3);                                    // Read humi_h3 (uint8)
    read_word_s(0xE1, humi_trim, address, 0xE2);                                     // Read humi_h2
    read_master(0xE4, hum_buffer, address, 0xE7);                                  // Read registers with humi_h4 & h5 in split up version into buffer

    read_master(0xD0, &chipid, address, 0xD0);                                     // Read chipid
    ESP_ERROR_CHECK(err);

    humi_trim[1] = ( (uint16_t)hum_buffer[0] << 4);                                         // humi_h4: Lowest 4 bits are of E5, bits 5-12 are E4
    humi_trim[1] |= ( (uint16_t)hum_buffer[1] & 0b00001111 );

    humi_trim[2] = ( (uint16_t)hum_buffer[2] << 4);                                         // humi_h5: Lowest 4 bits are highest 4 of E5, bits 5-12 are E6
    humi_trim[2] |= ( (uint16_t)hum_buffer[1] & 0b11110000 ) >> 4;

    humi_h6      = hum_buffer[3];                                                 // humi_h6 is content of register 0xE7 as a signed char

    ESP_LOGD(TAG, "Read trimming values of chip.");

#if LOG_LOCAL_LEVEL == ESP_LOG_DEBUG

    ESP_LOGD(TAG, "Values: ");
    ESP_LOGD(TAG, "TEMP_T1: %d (%#06x)", temp_t1, temp_t1);
    for (int i = 0; i < (sizeof(temp_trim) / sizeof(temp_trim[0]) ); i++)
    {
        ESP_LOGD(TAG, "TEMP_T%d: %d (%#06x)", i+2, temp_trim[i], temp_trim[i]);
    }
    ESP_LOGD(TAG, "PRES_P1: %d (%#06x)", pres_p1, pres_p1);
    for (int i = 0; i < (sizeof(pres_trim) / sizeof(pres_trim[0]) ); i++)
    {
        ESP_LOGD(TAG, "PRES_P%d: %d (%#06x)", i+2, pres_trim[i], pres_trim[i]);
    }
    ESP_LOGD(TAG, "HUMI_H1: %d (%#06x)", humi_h1, humi_h1);
    ESP_LOGD(TAG, "HUMI_H2: %d (%#06x)", humi_trim[0], humi_trim[0]);
    ESP_LOGD(TAG, "HUMI_H3: %d (%#06x)", humi_h3, humi_h3);
    ESP_LOGD(TAG, "HUMI_H4: %d (%#06x)", humi_trim[1], humi_trim[1]);
    ESP_LOGD(TAG, "HUMI_H5: %d (%#06x)", humi_trim[2], humi_trim[2]);
    ESP_LOGD(TAG, "HUMI_H6: %d (%#06x)", humi_h6, humi_h6);

#endif // List trimming parameters
    
    if (err == ESP_OK) ESP_LOGI(TAG,"Successfully initialized BME280 with chip id %#04x.", chipid);
    init_success = true;

    // Copy trim parameters into trim structure
    trim_params.dig_t1 = temp_t1;
    trim_params.dig_t2 = temp_trim[0];
    trim_params.dig_t3 = temp_trim[1];

    trim_params.dig_p1 = pres_p1;
    trim_params.dig_p2 = pres_trim[0];
    trim_params.dig_p3 = pres_trim[1];
    trim_params.dig_p4 = pres_trim[2];
    trim_params.dig_p5 = pres_trim[3];
    trim_params.dig_p6 = pres_trim[4];
    trim_params.dig_p7 = pres_trim[5];
    trim_params.dig_p8 = pres_trim[6];
    trim_params.dig_p9 = pres_trim[7];

    trim_params.dig_h1 = humi_h1;
    trim_params.dig_h2 = humi_trim[0];
    trim_params.dig_h3 = humi_h3;
    trim_params.dig_h4 = humi_trim[1];
    trim_params.dig_h5 = humi_trim[2];
    trim_params.dig_h6 = humi_h6;


    return err;
}

esp_err_t read_BME(uint8_t address, measureptr storage)
{
    uint8_t buffer[3];
    struct bme280_uncomp_data readout;
    esp_err_t err = ESP_OK;

    if (!init_success) ESP_ERROR_CHECK(init_BME(address));        // initialize sensor if it hasn't been already
    // trigger measurement through forced mode, then wait until sensor is ready
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // indicate WRITE to BME280
    write_master(&cmd, 0xF4, (MEASURE_SETTINGS | TAKE_MEASURE) );               // write Measurement settings + forced mode to BME
    i2c_master_stop(cmd);
    err = i2c_exec_cmd(&cmd);
    i2c_cmd_link_delete(cmd);
    ESP_ERROR_CHECK(err);

    vTaskDelay(7 / portTICK_PERIOD_MS); // typical time for measurement is supposed to be 7 ms, checking after if measurement takes too long
    check_ready(address);
    
    // read raw data into buffer, then buffer into readout
    // temperature
    read_master(0xFA,buffer,address,0xFC);
    readout.temperature = ( ((uint32_t)buffer[0] << 12) | ((uint32_t)buffer[1] << 4) | ((uint32_t)buffer[2] >> 4 ) );
    // pressure
    read_master(0xF7,buffer,address,0xF9);
    readout.pressure = ( ((uint32_t)buffer[0] << 12) | ((uint32_t)buffer[1] << 4) | ((uint32_t)buffer[2] >> 4 ) );
    // humidity
    read_master(0xFD,buffer,address,0xFE);
    readout.humidity = ( ((uint32_t)buffer[0] << 8) | (uint32_t)buffer[1] );

    ESP_LOGV(TAG, "Raw temp: %d, Raw pressure: %d, Raw humidity: %d", readout.temperature, readout.pressure, readout.humidity);


    // readout data is uncompensated, thus real measurements needs to be calculated by following functions
    storage->temperature    = calc_temp(&trim_params, &readout);
    storage->pressure    = calc_pres(&trim_params, &readout);
    storage->humidity    = calc_humi(&trim_params, &readout);
    ESP_LOGV(TAG, "t_fine = %d C", trim_params.t_fine);
    ESP_LOGD(TAG, "Raw measurement results: \n\t Temp: %d (/ 100) C \n\t Pres: %d (/100) Pa \n\t Humi: %d (/1024) %%", storage->temperature, storage->pressure, storage->humidity);
    return err;
}