#include "BME280_trim.h"

// Compensation formulas from https://github.com/BoschSensortec/BME280_driver/

int32_t calc_temp(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata)    // entspricht compensate_temperature
{
    int32_t var1;
    int32_t var2;
    int32_t temperature;
    int32_t temperature_min = -4000;
    int32_t temperature_max = 8500;

    var1 = (int32_t)((rawdata->temperature / 8) - ((int32_t)trim_data->dig_t1 * 2));
    var1 = (var1 * ((int32_t)trim_data->dig_t2)) / 2048;
    var2 = (int32_t)((rawdata->temperature / 16) - ((int32_t)trim_data->dig_t1));
    var2 = (((var2 * var2) / 4096) * ((int32_t)trim_data->dig_t3)) / 16384;
    trim_data->t_fine = (var1 + var2);
    temperature = (trim_data->t_fine * 5 + 128) / 256;

    if (temperature < temperature_min)
    {
        temperature = temperature_min;
    }
    else if (temperature > temperature_max)
    {
        temperature = temperature_max;
    }
    
    return temperature;
}

uint32_t calc_pres(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata)
{
    int64_t var1;
    int64_t var2;
    int64_t var3;
    int64_t var4;
    uint32_t pressure;
    uint32_t pressure_min = 3000000;
    uint32_t pressure_max = 11000000;

    var1 = ((int64_t)trim_data->t_fine) - 128000;
    var2 = var1 * var1 * (int64_t)trim_data->dig_p6;
    var2 = var2 + ((var1 * (int64_t)trim_data->dig_p5) * 131072);
    var2 = var2 + (((int64_t)trim_data->dig_p4) * 34359738368);
    var1 = ((var1 * var1 * (int64_t)trim_data->dig_p3) / 256) + ((var1 * ((int64_t)trim_data->dig_p2) * 4096));
    var3 = ((int64_t)1) * 140737488355328;
    var1 = (var3 + var1) * ((int64_t)trim_data->dig_p1) / 8589934592;

    /* To avoid divide by zero exception */
    if (var1 != 0)
    {
        var4 = 1048576 - rawdata->pressure;
        var4 = (((var4 * INT64_C(2147483648)) - var2) * 3125) / var1;
        var1 = (((int64_t)trim_data->dig_p9) * (var4 / 8192) * (var4 / 8192)) / 33554432;
        var2 = (((int64_t)trim_data->dig_p8) * var4) / 524288;
        var4 = ((var4 + var1 + var2) / 256) + (((int64_t)trim_data->dig_p7) * 16);
        pressure = (uint32_t)(((var4 / 2) * 100) / 128);

        if (pressure < pressure_min)
        {
            pressure = pressure_min;
        }
        else if (pressure > pressure_max)
        {
            pressure = pressure_max;
        }
    }
    else
    {
        pressure = pressure_min;
    }

    return pressure;
}

uint32_t calc_humi(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata)
{
    int32_t var1;
    int32_t var2;
    int32_t var3;
    int32_t var4;
    int32_t var5;
    uint32_t humidity;
    uint32_t humidity_max = 102400;

    var1 = trim_data->t_fine - ((int32_t)76800);
    var2 = (int32_t)(rawdata->humidity * 16384);
    var3 = (int32_t)(((int32_t)trim_data->dig_h4) * 1048576);
    var4 = ((int32_t)trim_data->dig_h5) * var1;
    var5 = (((var2 - var3) - var4) + (int32_t)16384) / 32768;
    var2 = (var1 * ((int32_t)trim_data->dig_h6)) / 1024;
    var3 = (var1 * ((int32_t)trim_data->dig_h3)) / 2048;
    var4 = ((var2 * (var3 + (int32_t)32768)) / 1024) + (int32_t)2097152;
    var2 = ((var4 * ((int32_t)trim_data->dig_h2)) + 8192) / 16384;
    var3 = var5 * var2;
    var4 = ((var3 / 32768) * (var3 / 32768)) / 128;
    var5 = var3 - ((var4 * ((int32_t)trim_data->dig_h1)) / 16);
    var5 = (var5 < 0 ? 0 : var5);
    var5 = (var5 > 419430400 ? 419430400 : var5);
    humidity = (uint32_t)(var5 / 4096);

    if (humidity > humidity_max)
    {
        humidity = humidity_max;
    }

    return humidity;
}