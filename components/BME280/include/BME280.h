#ifndef BME280_H
#define BME280_H

#include "esp_err.h"
#include <stdint.h>
#include <time.h>

typedef struct measurement
{
    int32_t temperature;
    uint32_t pressure;
    uint32_t humidity;

    time_t timestamp;
    uint8_t device_id[6];

    struct measurement *next;
} measure;
typedef struct measurement* measureptr;

esp_err_t init_BME(uint8_t address);
esp_err_t read_BME(uint8_t address, measureptr storage);

#endif //BME208_H