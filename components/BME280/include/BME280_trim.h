#ifndef BME280_TRIM_H
#define BME280_TRIM_H

#include <stdint.h>

// from https://github.com/BoschSensortec/BME280_driver/
struct bme280_calib_data
{
    /*< Calibration coefficient for the temperature sensor */
    uint16_t dig_t1;

    /*< Calibration coefficient for the temperature sensor */
    int16_t dig_t2;

    /*< Calibration coefficient for the temperature sensor */
    int16_t dig_t3;

    /*< Calibration coefficient for the pressure sensor */
    uint16_t dig_p1;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p2;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p3;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p4;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p5;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p6;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p7;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p8;

    /*< Calibration coefficient for the pressure sensor */
    int16_t dig_p9;

    /*< Calibration coefficient for the humidity sensor */
    uint8_t dig_h1;

    /*< Calibration coefficient for the humidity sensor */
    int16_t dig_h2;

    /*< Calibration coefficient for the humidity sensor */
    uint8_t dig_h3;

    /*< Calibration coefficient for the humidity sensor */
    int16_t dig_h4;

    /*< Calibration coefficient for the humidity sensor */
    int16_t dig_h5;

    /*< Calibration coefficient for the humidity sensor */
    int8_t dig_h6;

    /*< Variable to store the intermediate temperature coefficient */
    int32_t t_fine;
};

struct bme280_uncomp_data
{
    /*< un-compensated pressure */
    uint32_t pressure;

    /*< un-compensated temperature */
    uint32_t temperature;

    /*< un-compensated humidity */
    uint32_t humidity;
};


int32_t calc_temp(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata);
uint32_t calc_pres(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata);
uint32_t calc_humi(struct bme280_calib_data *trim_data, struct bme280_uncomp_data *rawdata);

#endif // BME280_TRIM_H