// 5x7 font (in 6x8 cell)
const unsigned char ucSmallFont6x8[] = {
    0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x3e,0x45,0x51,0x45,0x3e,
    0x00,0x3e,0x6b,0x6f,0x6b,0x3e,
    0x00,0x1c,0x3e,0x7c,0x3e,0x1c,
    0x00,0x18,0x3c,0x7e,0x3c,0x18,
    0x00,0x30,0x36,0x7f,0x36,0x30,
    0x00,0x18,0x5c,0x7e,0x5c,0x18,
    0x00,0x00,0x18,0x18,0x00,0x00,
    0x00,0xff,0xe7,0xe7,0xff,0xff,
    0x00,0x3c,0x24,0x24,0x3c,0x00,
    0x00,0xc3,0xdb,0xdb,0xc3,0xff,
    0x00,0x30,0x48,0x4a,0x36,0x0e,
    0x00,0x06,0x29,0x79,0x29,0x06,
    0x00,0x60,0x70,0x3f,0x02,0x04,
    0x00,0x60,0x7e,0x0a,0x35,0x3f,
    0x00,0x2a,0x1c,0x36,0x1c,0x2a,
    0x00,0x00,0x7f,0x3e,0x1c,0x08,
    0x00,0x08,0x1c,0x3e,0x7f,0x00,
    0x00,0x14,0x36,0x7f,0x36,0x14,
    0x00,0x00,0x5f,0x00,0x5f,0x00,
    0x00,0x06,0x09,0x7f,0x01,0x7f,
    0x00,0x22,0x4d,0x55,0x59,0x22,
    0x00,0x60,0x60,0x60,0x60,0x00,
    0x00,0x14,0xb6,0xff,0xb6,0x14,
    0x00,0x04,0x06,0x7f,0x06,0x04,
    0x00,0x10,0x30,0x7f,0x30,0x10,
    0x00,0x08,0x08,0x3e,0x1c,0x08,
    0x00,0x08,0x1c,0x3e,0x08,0x08,
    0x00,0x78,0x40,0x40,0x40,0x40,
    0x00,0x08,0x3e,0x08,0x3e,0x08,
    0x00,0x30,0x3c,0x3f,0x3c,0x30,
    0x00,0x03,0x0f,0x3f,0x0f,0x03,
    0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x06,0x5f,0x06,0x00,
    0x00,0x07,0x03,0x00,0x07,0x03,
    0x00,0x24,0x7e,0x24,0x7e,0x24,
    0x00,0x24,0x2b,0x6a,0x12,0x00,
    0x00,0x63,0x13,0x08,0x64,0x63,
    0x00,0x36,0x49,0x56,0x20,0x50,
    0x00,0x00,0x07,0x03,0x00,0x00,
    0x00,0x00,0x3e,0x41,0x00,0x00,
    0x00,0x00,0x41,0x3e,0x00,0x00,
    0x00,0x08,0x3e,0x1c,0x3e,0x08,
    0x00,0x08,0x08,0x3e,0x08,0x08,
    0x00,0x00,0xe0,0x60,0x00,0x00,
    0x00,0x08,0x08,0x08,0x08,0x08,
    0x00,0x00,0x60,0x60,0x00,0x00,
    0x00,0x20,0x10,0x08,0x04,0x02,
    0x00,0x3e,0x51,0x49,0x45,0x3e,
    0x00,0x00,0x42,0x7f,0x40,0x00,
    0x00,0x62,0x51,0x49,0x49,0x46,
    0x00,0x22,0x49,0x49,0x49,0x36,
    0x00,0x18,0x14,0x12,0x7f,0x10,
    0x00,0x2f,0x49,0x49,0x49,0x31,
    0x00,0x3c,0x4a,0x49,0x49,0x30,
    0x00,0x01,0x71,0x09,0x05,0x03,
    0x00,0x36,0x49,0x49,0x49,0x36,
    0x00,0x06,0x49,0x49,0x29,0x1e,
    0x00,0x00,0x6c,0x6c,0x00,0x00,
    0x00,0x00,0xec,0x6c,0x00,0x00,
    0x00,0x08,0x14,0x22,0x41,0x00,
    0x00,0x24,0x24,0x24,0x24,0x24,
    0x00,0x00,0x41,0x22,0x14,0x08,
    0x00,0x02,0x01,0x59,0x09,0x06,
    0x00,0x3e,0x41,0x5d,0x55,0x1e,
    0x00,0x7e,0x11,0x11,0x11,0x7e,
    0x00,0x7f,0x49,0x49,0x49,0x36,
    0x00,0x3e,0x41,0x41,0x41,0x22,
    0x00,0x7f,0x41,0x41,0x41,0x3e,
    0x00,0x7f,0x49,0x49,0x49,0x41,
    0x00,0x7f,0x09,0x09,0x09,0x01,
    0x00,0x3e,0x41,0x49,0x49,0x7a,
    0x00,0x7f,0x08,0x08,0x08,0x7f,
    0x00,0x00,0x41,0x7f,0x41,0x00,
    0x00,0x30,0x40,0x40,0x40,0x3f,
    0x00,0x7f,0x08,0x14,0x22,0x41,
    0x00,0x7f,0x40,0x40,0x40,0x40,
    0x00,0x7f,0x02,0x04,0x02,0x7f,
    0x00,0x7f,0x02,0x04,0x08,0x7f,
    0x00,0x3e,0x41,0x41,0x41,0x3e,
    0x00,0x7f,0x09,0x09,0x09,0x06,
    0x00,0x3e,0x41,0x51,0x21,0x5e,
    0x00,0x7f,0x09,0x09,0x19,0x66,
    0x00,0x26,0x49,0x49,0x49,0x32,
    0x00,0x01,0x01,0x7f,0x01,0x01,
    0x00,0x3f,0x40,0x40,0x40,0x3f,
    0x00,0x1f,0x20,0x40,0x20,0x1f,
    0x00,0x3f,0x40,0x3c,0x40,0x3f,
    0x00,0x63,0x14,0x08,0x14,0x63,
    0x00,0x07,0x08,0x70,0x08,0x07,
    0x00,0x71,0x49,0x45,0x43,0x00,
    0x00,0x00,0x7f,0x41,0x41,0x00,
    0x00,0x02,0x04,0x08,0x10,0x20,
    0x00,0x00,0x41,0x41,0x7f,0x00,
    0x00,0x04,0x02,0x01,0x02,0x04,
    0x00,0x80,0x80,0x80,0x80,0x80,
    0x00,0x00,0x03,0x07,0x00,0x00,
    0x00,0x20,0x54,0x54,0x54,0x78,
    0x00,0x7f,0x44,0x44,0x44,0x38,
    0x00,0x38,0x44,0x44,0x44,0x28,
    0x00,0x38,0x44,0x44,0x44,0x7f,
    0x00,0x38,0x54,0x54,0x54,0x08,
    0x00,0x08,0x7e,0x09,0x09,0x00,
    0x00,0x18,0xa4,0xa4,0xa4,0x7c,
    0x00,0x7f,0x04,0x04,0x78,0x00,
    0x00,0x00,0x00,0x7d,0x40,0x00,
    0x00,0x40,0x80,0x84,0x7d,0x00,
    0x00,0x7f,0x10,0x28,0x44,0x00,
    0x00,0x00,0x00,0x7f,0x40,0x00,
    0x00,0x7c,0x04,0x18,0x04,0x78,
    0x00,0x7c,0x04,0x04,0x78,0x00,
    0x00,0x38,0x44,0x44,0x44,0x38,
    0x00,0xfc,0x44,0x44,0x44,0x38,
    0x00,0x38,0x44,0x44,0x44,0xfc,
    0x00,0x44,0x78,0x44,0x04,0x08,
    0x00,0x08,0x54,0x54,0x54,0x20,
    0x00,0x04,0x3e,0x44,0x24,0x00,
    0x00,0x3c,0x40,0x20,0x7c,0x00,
    0x00,0x1c,0x20,0x40,0x20,0x1c,
    0x00,0x3c,0x60,0x30,0x60,0x3c,
    0x00,0x6c,0x10,0x10,0x6c,0x00,
    0x00,0x9c,0xa0,0x60,0x3c,0x00,
    0x00,0x64,0x54,0x54,0x4c,0x00,
    0x00,0x08,0x3e,0x41,0x41,0x00,
    0x00,0x00,0x00,0x77,0x00,0x00,
    0x00,0x00,0x41,0x41,0x3e,0x08,
    0x00,0x02,0x01,0x02,0x01,0x00,
    0x00,0x3c,0x26,0x23,0x26,0x3c
};


//WARNING: This Font Require X-GLCD Lib.
//         You can not use it with MikroE GLCD Lib.

//Font Generated by MikroElektronika GLCD Font Creator 1.2.0.0
//MikroElektronika 2011 
//http://www.mikroe.com 

//GLCD FontName : Calibri16x16
//GLCD FontSize : 16 x 16

#define WIDTH 16
#define HEIGHT 16

const unsigned char Calibri16x16[] = {
		0x10, 0x10,																							// Width and height
        0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char  
        0x04, 0x00, 0x00, 0x00, 0x00, 0xFE, 0x37, 0xFE, 0x37, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char !
        0x07, 0x00, 0x00, 0x3E, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char "
        0x09, 0x20, 0x04, 0x20, 0x3F, 0xFC, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x3F, 0xFC, 0x04, 0x20, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char #
        0x09, 0x00, 0x00, 0x70, 0x10, 0xF8, 0x30, 0xCC, 0xE0, 0x84, 0xE1, 0x87, 0x21, 0x07, 0x33, 0x0C, 0x1F, 0x08, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char $
        0x0D, 0x00, 0x00, 0x78, 0x00, 0x84, 0x20, 0x84, 0x10, 0x84, 0x08, 0x78, 0x06, 0x00, 0x01, 0x80, 0x00, 0x60, 0x1E, 0x10, 0x21, 0x08, 0x21, 0x04, 0x21, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char %
        0x0C, 0x00, 0x00, 0x00, 0x0E, 0x3C, 0x1F, 0xFE, 0x31, 0xC2, 0x20, 0xC2, 0x21, 0x62, 0x23, 0x3E, 0x16, 0x1C, 0x1C, 0x80, 0x1F, 0x80, 0x33, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char &
        0x03, 0x00, 0x00, 0x3E, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char '
        0x05, 0x00, 0x00, 0xE0, 0x1F, 0xFC, 0xFF, 0x1E, 0xE0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char (
        0x05, 0x00, 0x00, 0x02, 0x00, 0x1E, 0xE0, 0xFC, 0xFF, 0xE0, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char )
        0x08, 0x28, 0x00, 0x6C, 0x00, 0x28, 0x00, 0xFE, 0x00, 0xFE, 0x00, 0x28, 0x00, 0x6C, 0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char *
        0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0xF0, 0x1F, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char +
        0x04, 0x00, 0x00, 0x00, 0x80, 0x00, 0xF0, 0x00, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ,
        0x05, 0x00, 0x00, 0x00, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char -
        0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char .
        0x07, 0x00, 0x80, 0x00, 0xF0, 0x00, 0x7F, 0xE0, 0x0F, 0xFC, 0x00, 0x1F, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char /
        0x09, 0x00, 0x00, 0xF0, 0x0F, 0xF8, 0x1F, 0x0C, 0x30, 0x04, 0x20, 0x04, 0x20, 0x0C, 0x30, 0xF8, 0x1F, 0xF0, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 0
        0x09, 0x00, 0x00, 0x10, 0x20, 0x18, 0x20, 0x0C, 0x20, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 1
        0x09, 0x00, 0x00, 0x08, 0x20, 0x0C, 0x30, 0x04, 0x38, 0x04, 0x2C, 0x04, 0x26, 0x8C, 0x23, 0xF8, 0x21, 0x70, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 2
        0x09, 0x00, 0x00, 0x08, 0x10, 0x0C, 0x30, 0x84, 0x20, 0x84, 0x20, 0x84, 0x20, 0xCC, 0x31, 0x78, 0x1F, 0x30, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 3
        0x0A, 0x00, 0x00, 0x00, 0x06, 0x80, 0x07, 0xE0, 0x04, 0x38, 0x04, 0x0C, 0x04, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 4
        0x09, 0x00, 0x00, 0x00, 0x10, 0xFC, 0x30, 0xFC, 0x20, 0x84, 0x20, 0x84, 0x20, 0x84, 0x31, 0x04, 0x1F, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 5
        0x09, 0x00, 0x00, 0xC0, 0x0F, 0xF0, 0x1F, 0xB8, 0x30, 0x8C, 0x20, 0x84, 0x20, 0x84, 0x31, 0x04, 0x1F, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 6
        0x09, 0x00, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x30, 0x04, 0x3E, 0x84, 0x0F, 0xF4, 0x01, 0x7C, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 7
        0x09, 0x00, 0x00, 0x30, 0x0E, 0x78, 0x1F, 0xCC, 0x31, 0x84, 0x20, 0x84, 0x20, 0xCC, 0x31, 0x78, 0x1F, 0x30, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 8
        0x09, 0x00, 0x00, 0x70, 0x20, 0xF8, 0x20, 0x8C, 0x21, 0x04, 0x21, 0x04, 0x31, 0x0C, 0x1D, 0xF8, 0x0F, 0xF0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 9
        0x04, 0x00, 0x00, 0x00, 0x00, 0x60, 0x30, 0x60, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char :
        0x04, 0x00, 0x00, 0x00, 0x80, 0x60, 0xF0, 0x60, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ;
        0x08, 0x00, 0x00, 0x00, 0x01, 0x80, 0x02, 0x80, 0x02, 0x40, 0x04, 0x20, 0x08, 0x20, 0x08, 0x10, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char <
        0x08, 0x00, 0x00, 0x80, 0x04, 0x80, 0x04, 0x80, 0x04, 0x80, 0x04, 0x80, 0x04, 0x80, 0x04, 0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char =
        0x08, 0x00, 0x00, 0x10, 0x10, 0x20, 0x08, 0x20, 0x08, 0x40, 0x04, 0x80, 0x02, 0x80, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char >
        0x08, 0x00, 0x00, 0x04, 0x00, 0x02, 0x00, 0x82, 0x37, 0x82, 0x37, 0xC6, 0x00, 0x7C, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ?
        0x0F, 0x00, 0x00, 0x80, 0x1F, 0xE0, 0x7F, 0x70, 0x60, 0x98, 0xC7, 0xC8, 0x8F, 0x64, 0x88, 0x24, 0x88, 0x24, 0x84, 0x44, 0x87, 0xE4, 0x4F, 0xEC, 0x08, 0x18, 0x0C, 0xF8, 0x07, 0xE0, 0x03, 0x00, 0x00,  // Code for char @
        0x0B, 0x00, 0x30, 0x00, 0x3E, 0x80, 0x0F, 0xF0, 0x05, 0x7C, 0x04, 0x0C, 0x04, 0x7C, 0x04, 0xF0, 0x05, 0x80, 0x0F, 0x00, 0x3E, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char A
        0x09, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x84, 0x20, 0x84, 0x20, 0x84, 0x20, 0xFC, 0x31, 0x38, 0x1F, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char B
        0x0A, 0x00, 0x00, 0xE0, 0x07, 0xF8, 0x1F, 0x18, 0x18, 0x0C, 0x30, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x08, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char C
        0x0B, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x0C, 0x30, 0x18, 0x18, 0xF8, 0x1F, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char D
        0x08, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x84, 0x20, 0x84, 0x20, 0x84, 0x20, 0x84, 0x20, 0x04, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char E
        0x08, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x84, 0x00, 0x84, 0x00, 0x84, 0x00, 0x84, 0x00, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char F
        0x0B, 0x00, 0x00, 0xE0, 0x07, 0xF8, 0x1F, 0x18, 0x18, 0x0C, 0x30, 0x04, 0x20, 0x84, 0x20, 0x84, 0x20, 0x84, 0x20, 0x84, 0x3F, 0x88, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char G
        0x0B, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char H
        0x04, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char I
        0x05, 0x00, 0x10, 0x00, 0x20, 0x00, 0x20, 0xFC, 0x3F, 0xFC, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char J
        0x09, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x80, 0x00, 0xC0, 0x03, 0x70, 0x07, 0x38, 0x1C, 0x0C, 0x38, 0x04, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char K
        0x08, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char L
        0x0F, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x1C, 0x00, 0xF0, 0x00, 0xC0, 0x03, 0x00, 0x0E, 0x00, 0x38, 0x00, 0x38, 0x00, 0x0E, 0xC0, 0x03, 0xF0, 0x00, 0x1C, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x00,  // Code for char M
        0x0B, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x0C, 0x00, 0x78, 0x00, 0xE0, 0x01, 0x80, 0x07, 0x00, 0x1E, 0x00, 0x30, 0xFC, 0x3F, 0xFC, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char N
        0x0C, 0x00, 0x00, 0xE0, 0x07, 0xF8, 0x1F, 0x18, 0x18, 0x0C, 0x30, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x0C, 0x30, 0x18, 0x18, 0xF8, 0x1F, 0xE0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char O
        0x09, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x04, 0x01, 0x04, 0x01, 0x04, 0x01, 0x8C, 0x01, 0xF8, 0x00, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char P
        0x0D, 0x00, 0x00, 0xE0, 0x07, 0xF8, 0x1F, 0x18, 0x18, 0x0C, 0x30, 0x04, 0x20, 0x04, 0x20, 0x04, 0x20, 0x0C, 0x30, 0x18, 0x78, 0xF8, 0x5F, 0xE0, 0xC7, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Q
        0x0A, 0x00, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x04, 0x01, 0x04, 0x01, 0x04, 0x01, 0x8C, 0x07, 0xF8, 0x1E, 0x70, 0x38, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char R
        0x08, 0x00, 0x00, 0x70, 0x10, 0xF8, 0x30, 0xCC, 0x20, 0x84, 0x21, 0x84, 0x33, 0x0C, 0x1F, 0x08, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char S
        0x08, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0xFC, 0x3F, 0xFC, 0x3F, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char T
        0x0B, 0x00, 0x00, 0xFC, 0x0F, 0xFC, 0x1F, 0x00, 0x30, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x30, 0xFC, 0x1F, 0xFC, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char U
        0x0B, 0x0C, 0x00, 0x7C, 0x00, 0xF0, 0x01, 0x80, 0x0F, 0x00, 0x3E, 0x00, 0x30, 0x00, 0x3E, 0x80, 0x0F, 0xF0, 0x01, 0x7C, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char V
        0x10, 0x00, 0x00, 0x1C, 0x00, 0xFC, 0x01, 0xE0, 0x0F, 0x00, 0x3E, 0x00, 0x38, 0x80, 0x0F, 0xF0, 0x03, 0x7C, 0x00, 0xF0, 0x03, 0x80, 0x0F, 0x00, 0x38, 0x00, 0x3E, 0xE0, 0x0F, 0xFC, 0x01, 0x1C, 0x00,  // Code for char W
        0x09, 0x00, 0x00, 0x0C, 0x30, 0x3C, 0x3C, 0x70, 0x0E, 0xC0, 0x03, 0xC0, 0x03, 0x70, 0x0E, 0x3C, 0x3C, 0x0C, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char X
        0x08, 0x0C, 0x00, 0x7C, 0x00, 0xF0, 0x01, 0x80, 0x3F, 0x80, 0x3F, 0xF0, 0x01, 0x7C, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Y
        0x09, 0x04, 0x30, 0x04, 0x38, 0x04, 0x2E, 0x04, 0x27, 0x84, 0x21, 0xE4, 0x20, 0x74, 0x20, 0x1C, 0x20, 0x0C, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Z
        0x05, 0x00, 0x00, 0xFE, 0xFF, 0xFE, 0xFF, 0x02, 0x80, 0x02, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char [
        0x07, 0x03, 0x00, 0x1F, 0x00, 0xFC, 0x00, 0xE0, 0x0F, 0x00, 0x7F, 0x00, 0xF0, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char BackSlash
        0x05, 0x00, 0x00, 0x02, 0x00, 0x02, 0x00, 0xFE, 0xFF, 0xFE, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ]
        0x08, 0x00, 0x00, 0x80, 0x01, 0xF0, 0x01, 0x7C, 0x00, 0x0C, 0x00, 0x7C, 0x00, 0xF0, 0x01, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ^
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char _
        0x05, 0x00, 0x00, 0x02, 0x00, 0x06, 0x00, 0x0C, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char `
        0x08, 0x00, 0x00, 0x40, 0x1C, 0x60, 0x3E, 0x20, 0x22, 0x20, 0x22, 0x20, 0x12, 0xE0, 0x3F, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char a
        0x09, 0x00, 0x00, 0xFE, 0x3F, 0xFE, 0x3F, 0x40, 0x10, 0x20, 0x20, 0x20, 0x20, 0x60, 0x30, 0xC0, 0x1F, 0x80, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char b
        0x08, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x1F, 0x60, 0x30, 0x20, 0x20, 0x20, 0x20, 0x60, 0x30, 0x40, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char c
        0x09, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x1F, 0x60, 0x30, 0x20, 0x20, 0x20, 0x20, 0x40, 0x10, 0xFE, 0x3F, 0xFE, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char d
        0x08, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x1F, 0x60, 0x32, 0x20, 0x22, 0x20, 0x22, 0xE0, 0x23, 0xC0, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char e
        0x07, 0x00, 0x00, 0x20, 0x00, 0xFC, 0x3F, 0xFE, 0x3F, 0x22, 0x00, 0x22, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char f
        0x09, 0x00, 0x00, 0xC0, 0x6B, 0xE0, 0xFF, 0x20, 0x94, 0x20, 0x94, 0x20, 0x94, 0xE0, 0x97, 0xE0, 0xF3, 0x20, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char g
        0x09, 0x00, 0x00, 0xFE, 0x3F, 0xFE, 0x3F, 0x40, 0x00, 0x20, 0x00, 0x20, 0x00, 0x60, 0x00, 0xC0, 0x3F, 0x80, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char h
        0x03, 0x00, 0x00, 0xE6, 0x3F, 0xE6, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char i
        0x03, 0x00, 0x00, 0xE6, 0xFF, 0xE6, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char j
        0x08, 0x00, 0x00, 0xFE, 0x3F, 0xFE, 0x3F, 0x00, 0x03, 0x80, 0x07, 0xC0, 0x1C, 0x60, 0x38, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char k
        0x03, 0x00, 0x00, 0xFE, 0x3F, 0xFE, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char l
        0x0D, 0x00, 0x00, 0xE0, 0x3F, 0xE0, 0x3F, 0x40, 0x00, 0x20, 0x00, 0x20, 0x00, 0xE0, 0x3F, 0xC0, 0x3F, 0x40, 0x00, 0x20, 0x00, 0x20, 0x00, 0xE0, 0x3F, 0xC0, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char m
        0x09, 0x00, 0x00, 0xE0, 0x3F, 0xE0, 0x3F, 0x40, 0x00, 0x20, 0x00, 0x20, 0x00, 0x60, 0x00, 0xC0, 0x3F, 0x80, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char n
        0x09, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x1F, 0x60, 0x30, 0x20, 0x20, 0x20, 0x20, 0x60, 0x30, 0xC0, 0x1F, 0x80, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char o
        0x09, 0x00, 0x00, 0xE0, 0xFF, 0xE0, 0xFF, 0x40, 0x10, 0x20, 0x20, 0x20, 0x20, 0x60, 0x30, 0xC0, 0x1F, 0x80, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char p
        0x09, 0x00, 0x00, 0x80, 0x0F, 0xC0, 0x1F, 0x60, 0x30, 0x20, 0x20, 0x20, 0x20, 0x40, 0x10, 0xE0, 0xFF, 0xE0, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char q
        0x06, 0x00, 0x00, 0xE0, 0x3F, 0xE0, 0x3F, 0x40, 0x00, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char r
        0x07, 0x00, 0x00, 0xC0, 0x11, 0xE0, 0x23, 0x20, 0x23, 0x20, 0x26, 0x20, 0x3E, 0x40, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char s
        0x06, 0x00, 0x00, 0x20, 0x00, 0xF8, 0x1F, 0xF8, 0x3F, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char t
        0x09, 0x00, 0x00, 0xE0, 0x0F, 0xE0, 0x1F, 0x00, 0x30, 0x00, 0x20, 0x00, 0x20, 0x00, 0x10, 0xE0, 0x3F, 0xE0, 0x3F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char u
        0x09, 0x60, 0x00, 0xE0, 0x03, 0x80, 0x0F, 0x00, 0x3C, 0x00, 0x30, 0x00, 0x3C, 0x80, 0x0F, 0xE0, 0x03, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char v
        0x0E, 0x60, 0x00, 0xE0, 0x03, 0x80, 0x0F, 0x00, 0x3C, 0x00, 0x38, 0x00, 0x0F, 0xE0, 0x01, 0xE0, 0x01, 0x00, 0x0F, 0x00, 0x38, 0x00, 0x3C, 0x80, 0x0F, 0xE0, 0x03, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char w
        0x08, 0x20, 0x20, 0xE0, 0x38, 0xC0, 0x1D, 0x00, 0x07, 0x00, 0x07, 0xC0, 0x1D, 0xE0, 0x38, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char x
        0x09, 0x60, 0x00, 0xE0, 0x03, 0x80, 0x0F, 0x00, 0xFC, 0x00, 0xF0, 0x00, 0x3C, 0x80, 0x0F, 0xE0, 0x03, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char y
        0x07, 0x00, 0x00, 0x20, 0x30, 0x20, 0x3C, 0x20, 0x2E, 0xA0, 0x23, 0xE0, 0x21, 0x60, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char z
        0x06, 0x00, 0x01, 0x80, 0x03, 0xFC, 0xFE, 0x7E, 0xFC, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char {
        0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFE, 0xFF, 0xFE, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char |
        0x06, 0x02, 0x80, 0x02, 0x80, 0x7E, 0xFC, 0xFC, 0x7E, 0x80, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char }
        0x08, 0x00, 0x00, 0x60, 0x00, 0x10, 0x00, 0x10, 0x00, 0x20, 0x00, 0x40, 0x00, 0x40, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ~
        0x04, 0xFC, 0x1F, 0x04, 0x10, 0x04, 0x10, 0xFC, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // Code for char 
        };
const unsigned char symbolsfont8x8[] = {						// 8x8 Font
	0x08, 0x08, 0x04, 0x12, 0xCA, 0xCA, 0x12, 0x04, 0x08,	// Full WiFi reception (3 bars) (elements 1-8)
	0x08, 0x00, 0x00, 0x10, 0xC8, 0xC8, 0x10, 0x00, 0x00,	// Medium WiFi reception (2 bars) (elements 9-16)
	0x08, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x00,	// Low WiFi reception (1 bar/base) (elements 17-24)
	0x08, 0x00, 0x00, 0x00, 0xDF, 0xDF, 0x00, 0x00, 0x00,	// No WiFi reception (exclamation mark) (elements 25-32)
    0x06, 0x00, 0x00, 0x6c, 0x6c, 0x00, 0x00, 0x00, 0x00,   // colon (from smallfont) (elements 33-40)
    0x07, 0x00, 0x44, 0x28, 0x10, 0x44, 0x28, 0x10, 0x00   // guillemets to the right (elements 41-48)
    //0x05, 0x00, 0x7E, 0x3C, 0x18, 0x00, 0x00, 0x00, 0x00 // arrow to the right (elements 41-48)
};
