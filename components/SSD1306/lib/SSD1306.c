#include "esp_log.h"
#include "esp_mesh.h"
#include "i2c_easycomms.h"

#include <time.h>
#include <string.h>

#include "SSD1306.h"
#include "SSD1306_gfx.h"

#define COMMANDBYTE 0b10000000
#define DATABYTE 0b11000000
#define CONTINUEBYTE 0

static const char *TAG = "SSD";
static bool init_success = false;
static uint8_t display[8][128] = {0};             // "pixels" of the display; organized in 8 pages with 8 rows each, and 128 segments/columns

static esp_err_t clear_display(uint8_t address, bool hard, uint8_t pagestart)
{
    esp_err_t err = ESP_OK;
    i2c_cmd_handle_t cmd;

    if (hard)   // clear screen and local display array
    {
        cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);
        // write_master(&cmd, COMMANDBYTE, 0xAE);                                      // turn display off
        for (uint8_t page = 0; page < 8; page++)
        {
            write_master(&cmd, COMMANDBYTE, (0xB0+page));
            for (uint8_t column = 0; column < 128; column++)
            {
                write_master(&cmd, DATABYTE, 0);
            }

        }
        // write_master(&cmd, COMMANDBYTE, 0xAF);                                      // turn display on, but not needed since display will need to be filled anyways
        i2c_master_stop(cmd);
        err = i2c_exec_cmd(&cmd);
        i2c_cmd_link_delete(cmd);
        ESP_ERROR_CHECK(err);

    }
    // clear local display array
    for(uint8_t page = (hard ? 0 : pagestart); page < 8; page++)    // if doing a "hard" clear, clean full local display ofc, else just from pagestart (for displaystatus)
    {
        for(uint8_t segment = 0; segment < 128; segment++)
        {
            display[page][segment] = 0;
        }
    }

    return err;    
}

static esp_err_t write_display(uint8_t address) // write entire local display to I2C display
{
    esp_err_t err = ESP_OK;
    i2c_cmd_handle_t cmd;

    // to lessen bytes needing to be sent: Get limits of first and last nonzero byte on display
    uint8_t printed[16];
    static uint8_t prev_printed[16] = {
        0, 127,
        0, 127,
        0, 127,
        0, 127,
        0, 127,
        0, 127,
        0, 127,
        0, 127
    };

    uint8_t column = 0;
    ESP_LOGV(TAG, "New print values:");
    for (uint8_t page = 0; page < 8; page++)
    {
        column = 0;
        while ( (display[page][column] == 0) && (column < 127) ) {
            column++;
        }
        printed[2 * page] = column;
        
        column = 127;
        while ( (display[page][column] == 0) && (column > printed[2*page]) ) {
            column--;
        }
        printed[2*page + 1] = column+1; // to set it to the first 0 byte
        if (printed[2*page + 1] >= 128)
            printed[2*page + 1] = 127; // Sanity check
        ESP_LOGV(TAG, "P%d: min %d, max %d", page, printed[2*page], printed[2*page+1]);
    }
    
    uint8_t minlimit, maxlimit;    
    
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);
    for (uint8_t page = 0; page < 8; page++)
    {
        // Set column limits: If page has been written in places "wider" than the current page, clear them --> start from there, and/or end there
        minlimit = ( (prev_printed[2*page] < printed[2*page]) ? prev_printed[2*page] : printed[2*page] );
        maxlimit = ( (prev_printed[2*page+1] > printed[2*page+1]) ? prev_printed[2*page+1] : printed[2*page+1] );
        
        write_master(&cmd, COMMANDBYTE, (0xB0 + page));
        write_master(&cmd, COMMANDBYTE, (minlimit & 0x0f) );
        write_master(&cmd, COMMANDBYTE, 0x10 | ( (minlimit & 0xf0) >> 4) ); 
        ESP_LOGV(TAG, "Writing page %d from %d to %d...", page, minlimit, maxlimit);

        // Write page in appropriate limits; the page will be incremented automatically after that
        for (uint8_t column = minlimit; column < maxlimit; column++)
        {
            write_master(&cmd, DATABYTE, display[page][column]);
        }
        
    }
    // write_master(&cmd, COMMANDBYTE, 0xAF);                                      // turn display on
    i2c_master_stop(cmd);
    err = i2c_exec_cmd(&cmd);
    i2c_cmd_link_delete(cmd);

    for (uint8_t i = 0; i < 16; i++)
    {
        prev_printed[i] = printed[i];
    }

    return err;
}

static esp_err_t splash_screen(uint8_t address)
{
    esp_err_t err = ESP_OK;

    // write "logo" into display; will not bother to clear the matrix as this function shall only be used as a test on startup
    // logo today: square, 32px side length
    for(uint8_t page = 0; page < 8; page++)
    {
        for(uint8_t column = 0; column < 128; column++)
        {
            if( ( (page > 1) && (page < 6) ) && ( (column >= 48) && (column <= 79) ) )
            {
                display[page][column] = 0xFFu;
            }
            else
            {
                display[page][column] = 0;
            }
        }
    }

    // adding: a 4px wide square with 64px side length (outside)
    for (uint8_t page = 0; page < 8; page++)
    {
        for (uint8_t column = 0; column < 128; column++)
        {
            if( ((column > 31) && (column < 36)) || ((column > 91) && (column < 96)) ){
                display[page][column] = 0xFFu;
            }
            else if( ((page == 0)) && ((column > 31) && (column < 96)) ){
                display[page][column] = 0x0Fu;
            }
            else if( ((page == 7)) && ((column > 31) && (column < 96)) ){
                display[page][column] = 0xF0u;
            }
            
        }
        
    }
    
    
    // write local display to real display
    // clear_display(address);    // just in case
    err = write_display(address);
    return err;
}

#ifdef CONFIG_SSD_BUTTON
static void disp_note(void *arg);
static void disp_toggle(void *arg);
static void IRAM_ATTR disp_isr(void *arg);
#endif

esp_err_t init_disp(uint8_t address)
{
    if (init_success) return ESP_OK;
    esp_err_t err = ESP_OK;
    i2c_cmd_handle_t cmd;

    ESP_ERROR_CHECK( init_i2c() ); // initialize I2C connection (if not initialized previously)

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // address SSD in writing mode
    // init with reset settings
    write_master(&cmd, COMMANDBYTE, 0xA8);                                      // set multiplex ratio to 63
    write_master(&cmd, COMMANDBYTE, 0x3F);
    write_master(&cmd, COMMANDBYTE, 0xD3);                                      // set standard offset to 0
    write_master(&cmd, COMMANDBYTE, 0);
    write_master(&cmd, COMMANDBYTE, 0x20);                                      // set addressing mode to page mode
    write_master(&cmd, COMMANDBYTE, 2);
    write_master(&cmd, COMMANDBYTE, 0x40);                                      // set start line to 0 (maybe change?), in bits: 0b01xxxxxx where xs are 0-63 in binary
#ifdef CONFIG_SSD_FLIP_SCREEN
    write_master(&cmd, COMMANDBYTE, 0xA1);                                      // segment remap to match flipped screen
    write_master(&cmd, COMMANDBYTE, 0xC8);                                      // COM scan in inverted direction (top to bottom)
#else
    write_master(&cmd, COMMANDBYTE, 0xA0);                                      // normal segment remap
    write_master(&cmd, COMMANDBYTE, 0xC0);                                      // COM scan in normal direction
#endif // Choose flipping screen
    write_master(&cmd, COMMANDBYTE, 0xDA);                                      // standard COM pin config
    write_master(&cmd, COMMANDBYTE, 0x12);
    write_master(&cmd, COMMANDBYTE, 0x81);                                      // standard max contrast (maybe add to config?)
    write_master(&cmd, COMMANDBYTE, 0x7F);
    //write_master(&cmd, COMMANDBYTE, 0xA5);                                      // turn entire display on for testing; remove later
    write_master(&cmd, COMMANDBYTE, 0xA4);                                      // no entire display on etc.
    write_master(&cmd, COMMANDBYTE, 0xA6);                                      // set display to normal mode
    write_master(&cmd, COMMANDBYTE, 0xD5);                                      // Set oscillator freq to standard 128 ?
    write_master(&cmd, COMMANDBYTE, 0x80);

    write_master(&cmd, COMMANDBYTE, 0x8D);                                      // enable charge pump?
    write_master(&cmd, COMMANDBYTE, 0x14);
    write_master(&cmd, COMMANDBYTE, 0xAF);                                      // turn display on
    i2c_master_stop(cmd);
    err = i2c_exec_cmd(&cmd);
    i2c_cmd_link_delete(cmd);
    ESP_ERROR_CHECK(err);
    init_success = true;
    ESP_LOGI(TAG,"Successfully initialized display SSD1306 at address %#04X.", address);
    splash_screen(address);
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    clear_display(address, true, 0);
    write_string("Waiting for", 21, 20, display, big_font);
    write_string("measurement...", 10, 36, display, big_font);
    err = write_display(address);
#ifdef CONFIG_SSD_BUTTON
    static esp_timer_handle_t dispoff = NULL;
    static TaskHandle_t toggler;

    esp_timer_create_args_t dispconf = {
        .name = "DISPOFF",
        .callback = disp_note,
        .arg = &toggler
    };
    esp_err_t ret = esp_timer_create(&dispconf, &dispoff);
    if (ret == ESP_ERR_NO_MEM)
    {
        while (ret == ESP_ERR_NO_MEM)
        {
            vTaskDelay(1 / portTICK_PERIOD_MS);
            ret = esp_timer_create(&dispconf,&dispoff);
        }

    }
    ESP_ERROR_CHECK(ret);
    xTaskCreate(disp_toggle, "DISPTGL", 2048, dispoff, 3, &toggler);
    esp_timer_start_once(dispoff, 10ULL * 1000000);
    
    gpio_config_t dispbutton = {
        .pin_bit_mask = (1ULL << CONFIG_SSD_BUTTON_PIN),
        .mode = GPIO_MODE_INPUT,
        .intr_type = GPIO_INTR_NEGEDGE,
        .pull_up_en = GPIO_PULLUP_ENABLE
    };
    gpio_config(&dispbutton);
    esp_err_t gpio = gpio_install_isr_service(ESP_INTR_FLAG_EDGE | ESP_INTR_FLAG_IRAM);
    if(gpio != ESP_OK)
    {
        if (gpio == ESP_ERR_INVALID_STATE)
        {
            ESP_LOGV(TAG, "ESP GPIO ISR already initialized. Ok...");
        }
        else if (gpio == ESP_ERR_NO_MEM)
        {
            for (uint8_t retries = 0; (gpio == ESP_ERR_NO_MEM) && (retries < 10); retries++)
            {
                vTaskDelay(100 / portTICK_PERIOD_MS);
                gpio = gpio_install_isr_service(ESP_INTR_FLAG_EDGE | ESP_INTR_FLAG_IRAM);
            }
            ESP_ERROR_CHECK(gpio);
        }
        else ESP_ERROR_CHECK(gpio);
    }
    ESP_ERROR_CHECK(gpio_isr_handler_add(CONFIG_SSD_BUTTON_PIN, disp_isr, toggler));
#endif
    return err;
}

esp_err_t display_measurement(uint8_t address, char *temp, char *humi, char *pres)
{
    esp_err_t err = ESP_OK;
    uint8_t fours_temp = 0, fours_humi = 0;     // '4' is 1 pixel wider than the other letters; need to pull x offset for every 2 '4's

    for (uint8_t letter = 0; temp[letter] != '\0'; letter++)
    {
        if(temp[letter] == '4') fours_temp++;
    }
    for (uint8_t letter = 0; humi[letter] != '\0'; letter++)
    {
        if(humi[letter] == '4') fours_humi++;
    }
     
       
    ESP_ERROR_CHECK( clear_display(address,false,1) );  // do not clear first page of display for displaystatus (in wifi branch)

    write_string(temp, 39 - fours_temp/2, 12, display,big_font); // todo: change offset seg
    write_string(humi, 38 - fours_humi/2, 28, display,big_font);
    write_string(pres, 34, 48, display,small_font);

    err = write_display(address);

    return err;
}

esp_err_t display_status(uint8_t address, bool mesh, uint8_t *id, char *name, int8_t rssi)
{
    esp_err_t err = ESP_OK;
    char offset, continuous;
    struct tm *timeexpanded = NULL;
    time_t timesmol = time(NULL);

    timeexpanded = localtime(&timesmol);
    char clock[6];
    strftime(clock,6,"%H:%M",timeexpanded);

    char devid[9] = {0,};
    if ( (strlen(name) <= 8) && (strlen(name) > 0) )
    {
        memcpy( (char *)devid, name, strlen(name) );
    }
    else
    {
        ESP_LOGD(TAG, "The provided custom name is longer than 8 characters. Using last bytes of mac address instead");
        sprintf(devid,"%02X:%02X",*id,*(id+1));
    }    

    for (uint8_t segment = 0; segment < 128; segment++)
    {
        display[0][segment] = 0;                            // clear first line locally only
    }
    
    
    if      (rssi >= -50) offset = 1;
    else if (rssi >= -75) offset = 2;
    else if (rssi >= -100)offset = 3;
    else offset = 4;
    char wifi[2] = {offset, '\0'};

    char punct[2];
    char conn[5];
    if (mesh)
    {
        mesh_addr_t parent;
         
        if (esp_mesh_is_root())
        {
            punct[0] = 5; punct[1] = 0;             // since using symbolfont, "char" 5 is a colon; could've used memcopy...
            memcpy((char *)conn, "ROOT", 5);
        }
        else
        {
            punct[0] = 6; punct[1] = 0;             // since using symbolfont, "char" 6 is an arrow; could've used memcopy...
            esp_mesh_get_parent_bssid(&parent);
            sprintf(conn,"%02X%02X",parent.addr[4],(parent.addr[5]-1));    // Why remove 1 from the last parent mac bit? Because the softAP address is 1 bigger than actual station address, which is preferred for identification
        }
    }
    else
    {
        punct[0] = '\0';
        conn[0] = '\0';
    }


    write_string(wifi, 0, 0, display, symbolfont);
    continuous = write_string(punct, 8, 0, display, symbolfont);
    write_string(conn, continuous, 1, display, small_font);
    write_string(devid, (64-(6*strlen(devid)/2)), 1, display, small_font);
    write_string(clock, 97, 1, display, small_font);
    err = write_display(address);
    
    return err;
}

#ifdef CONFIG_SSD_BUTTON
static void disp_note(void *arg)
{
    TaskHandle_t *toggle = (TaskHandle_t *)arg;
    xTaskNotifyGive(*toggle);
}

static void disp_toggle(void *arg)
{
    uint8_t dispon = 1;
    i2c_cmd_handle_t cmd;

    esp_timer_handle_t timer = (esp_timer_handle_t)arg;

    while(1)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        cmd = i2c_cmd_link_create();

        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (CONFIG_ADDRESS_SSD1306 << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // address SSD in writing mode
        write_master(&cmd, COMMANDBYTE, (0xAF - dispon) );
        i2c_master_stop(cmd);
        ESP_ERROR_CHECK(i2c_exec_cmd(&cmd));
        i2c_cmd_link_delete(cmd);
        ESP_LOGD(TAG, "Turned display %s.", (dispon ? "off" : "on") );

        if(dispon)
        {
            esp_timer_stop(timer);
            dispon = 0;
        }
        else
        {
            dispon = 1;
            esp_timer_start_once(timer, 10ULL * 1000000);
        }
    }
    vTaskDelete(NULL);
}

static void IRAM_ATTR disp_isr(void *arg)
{
    BaseType_t wakey = pdFALSE;
    
    TaskHandle_t toggle = (TaskHandle_t)arg;
    vTaskNotifyGiveFromISR(toggle, &wakey);

    if (wakey == pdTRUE) portYIELD_FROM_ISR();
}
#endif
