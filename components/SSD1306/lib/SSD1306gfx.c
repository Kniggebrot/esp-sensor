#include "esp_log.h"
//#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <string.h>

#include "SSD1306_gfx.h"

extern const unsigned char ucSmallFont6x8[];
extern const unsigned char Calibri16x16[];
extern const unsigned char symbolsfont8x8[];

static const char *TAG = "GFX";

static uint16_t fill_pixelchar(uint8_t width, uint8_t lineheight, uint8_t pixelchar[width][lineheight], char current, font fonttype)
{
    uint16_t start = 0;
    
    switch(fonttype)
    {
        case big_font:
            start = 2 + (width * lineheight + 1) * (current - 32);
            for (uint8_t i = 0; i < width; i++)
            {
                for (uint8_t j = 0; j < lineheight; j++)
                {
                    pixelchar[i][j] = Calibri16x16[start+j+lineheight*i+1];
                }
            }
            break;
        case small_font:
            start = width * lineheight * current;
            for (uint8_t i = 0; i < width; i++)
            {
                for (uint8_t j = 0; j < lineheight; j++)
                {
                    pixelchar[i][j] = ucSmallFont6x8[start+j+lineheight*i]; //+1
                }
            }
            break;
        case symbolfont:
            start = (width * lineheight + 1) * (current - 1);
            for (uint8_t i = 0; i < width; i++)
            {
                for (uint8_t j = 0; j < lineheight; j++)
                {
                    pixelchar[i][j] = symbolsfont8x8[start+j+lineheight*i+1];
                }
            }
            break;
        default:
            ESP_LOGE(TAG, "No valid font used! Please use a font from the enumeration.");
            return 0xFFFF;
            
    }
    
    return start;
    
}

uint8_t write_string(char *string, uint8_t offset_x, uint8_t offset_y, uint8_t display[8][128], font fonttype)
{
    uint8_t segment = offset_x, line = offset_y, currentbit = 0, width = 8, height = 8;
    switch (fonttype)
    {
        case big_font:
            width = Calibri16x16[0];
            height = Calibri16x16[1];
            break;
        case small_font:
            width = 6;
            height = 8;
            break;
        case symbolfont:
            break;          // width and height are already 8
        default:
            ESP_LOGE(TAG,"Invalid font selected!");
            break;

    }
    
    uint8_t pixelchar[width][height / 8];
    uint16_t width_index;
    for (uint8_t letter = 0; letter < strlen(string); letter++)
    {
        switch (fonttype)
        {
            case big_font:  
                width_index = fill_pixelchar(width, (height / 8), pixelchar, string[letter], fonttype);
                for (uint8_t charwidth = 0; ( ( charwidth < Calibri16x16[width_index] ) && (segment < 128)); charwidth++)   // one letter at a time 
                {
                    for (uint8_t charheight = 0; ( ( charheight < height ) && (line < 64) ); charheight++)
                    {
                        currentbit = ( pixelchar[charwidth][charheight / 8] & (1 << (charheight % 8) ) ? 1 : 0);
                        display[line / 8][segment] |= (currentbit << (line % 8));
                        line++;
                        currentbit = 0;
                    }
                    segment++;
                    line = offset_y; // start a new char always at the top!
                }
                break;
            case small_font:
                width_index = fill_pixelchar(width, (height / 8), pixelchar, string[letter], fonttype);
                for (uint8_t charwidth = 0; ( ( charwidth < width ) && (segment < 128)); charwidth++)   // one letter at a time 
                {
                    for (uint8_t charheight = 0; ( ( charheight < height ) && (line < 64) ); charheight++)
                    {
                        currentbit = ( pixelchar[charwidth][charheight / 8] & (1 << (charheight % 8) ) ? 1 : 0);
                        display[line / 8][segment] |= (currentbit << (line % 8));
                        line++;
                        currentbit = 0;
                    }
                    segment++;
                    line = offset_y; // start a new char always at the top!
                }
                break;
            case symbolfont:
                width_index = fill_pixelchar(width, (height / 8), pixelchar, string[letter], fonttype);
                for (uint8_t charwidth = 0; ( ( charwidth < symbolsfont8x8[width_index] ) && (segment < 128)); charwidth++)   // one letter at a time 
                {
                    for (uint8_t charheight = 0; ( ( charheight < height ) && (line < 64) ); charheight++)
                    {
                        currentbit = ( pixelchar[charwidth][charheight / 8] & (1 << (charheight % 8) ) ? 1 : 0);
                        display[line / 8][segment] |= (currentbit << (line % 8));
                        line++;
                        currentbit = 0;
                    }
                    segment++;
                    line = offset_y; // start a new char always at the top!
                }
                break;
            default:
                ESP_LOGE(TAG, "No valid font used for string \"%s\" !", string);
                ESP_LOGE(TAG, "Please use a font from the font enumeration.");
                return 0;                
        }
    }
    ESP_LOGV(TAG, "String %s \n\n\t occupies %d segments.", string, (segment - offset_x));
    return segment;
}
