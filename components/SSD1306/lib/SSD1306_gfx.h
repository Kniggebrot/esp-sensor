#ifndef SSD1306GFX_H /* Header guards */
#define SSD1306GFX_H
#include <stdint.h>

typedef enum {
    big_font = 0,
    small_font,
    symbolfont
} font;

uint8_t write_string(char *string, uint8_t offset_x, uint8_t offset_y, uint8_t display[8][128], font fonttype);   // will write string in local display array; still needs to be pushed out (with write_display()), also, one line only
#endif // SSD1306_H
