if(CONFIG_SSD_USE_DISPLAY)
     set(srcs "lib/fonts.c" "lib/SSD1306.c" "lib/SSD1306gfx.c")
else()
     set(srcs "")
endif()




idf_component_register(SRCS "${srcs}"   
                     INCLUDE_DIRS "include"
                     PRIV_INCLUDE_DIRS "lib"
                     PRIV_REQUIRES I2C_easycomms)