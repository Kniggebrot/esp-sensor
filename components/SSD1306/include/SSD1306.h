#ifndef SSD1306_H /* Header guards */
#define SSD1306_H

#include "esp_err.h"
#include <stdint.h>
#include <stdbool.h>

esp_err_t init_disp(uint8_t address);
esp_err_t display_measurement(uint8_t address, char *temp, char *humi, char *pres);
esp_err_t display_status(uint8_t address, bool mesh, uint8_t *id, char *name, int8_t rssi);

#endif // SSD1306_H
