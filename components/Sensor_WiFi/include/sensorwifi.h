// Wifi connection for sensor; for now, only "modern" esp-idf
#ifndef SENSORWIFI_H
#define SENSORWIFI_H

#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

esp_err_t init_wifi();
EventGroupHandle_t *get_wifi_events(); // get wifi event group from sensorwifi 
bool start_timesync();

#endif // SENSORWIFI_H