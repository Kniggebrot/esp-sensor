#ifndef SENSORMESH_H
#define SENSORMESH_H

#include <stdbool.h>
#include "esp_err.h"

esp_err_t init_mesh();
bool *get_mesh_conn();

#endif // SENSORMESH_H