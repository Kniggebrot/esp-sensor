#include <stdlib.h>

#include "esp_event.h"
#include "esp_log.h"
#include "esp_sntp.h"
#include "esp_wifi.h"
#include "lwip/err.h"
#include "lwip/sys.h"

#include "sdkconfig.h"

#include "sensorwifi.h"
const static char max_retries= 5;
static const char *TAG = "WIFI";

#ifndef CONFIG_MESH_ENABLE  // If mesh isn't enabled, build classic wifi functions

// WiFi module mostly based of example in esp-idf

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
static char s_retry_num = 0;

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        
        ESP_LOGW(TAG, "No WiFi network connected. Trying for (re)connect...");
        
        xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        esp_wifi_connect();

    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupClearBits(s_wifi_event_group, WIFI_FAIL_BIT);
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

esp_err_t init_wifi()
{
    
    char ssid[] = CONFIG_WIFI_SSID, pass[] = CONFIG_WIFI_PASS;
    if (ssid[0] == '\0' || pass[0] == '\0')
    {
        ESP_LOGE(TAG, "No WiFi SSID to connect to or password to use defined. Please enter it in the menuconfig under \"Component Settings -> Sensor: WiFi settings\".");
        return ESP_FAIL;
    }
    
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    esp_err_t err = esp_event_loop_create_default();
    if (err != ESP_OK)
    {
        if (err == ESP_ERR_INVALID_STATE)
        {
            ESP_LOGV(TAG, "Default event loop already created. Ok...");
        }
        else
        {
            ESP_ERROR_CHECK(err);
        }
        
    }
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASS,
            .threshold = {
                .authmode = WIFI_AUTH_WPA2_PSK,
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGD(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGD(TAG, "connected to ap");
        return ESP_OK;
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGD(TAG, "Failed to connect");
        return ESP_OK;                      // even on failed wifi connection measurements shall be made?
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT: %#X",bits);
        return ESP_FAIL;
    }
}

EventGroupHandle_t *get_wifi_events()
{
    return &s_wifi_event_group;
}

#else

EventGroupHandle_t *get_wifi_events()
{
    return NULL;
}

esp_err_t init_wifi()
{
    return ESP_FAIL;
}

#endif // Wifi functions if mesh isn't enabled

bool start_timesync()
{
    uint8_t retry = 0;
    static bool firstrun = true;

    if (firstrun)
    {
        setenv("TZ","CET-1CEST,M3.5.0/2,M10.5.0/2",1);
        tzset();
        sntp_setoperatingmode(SNTP_OPMODE_POLL);
        sntp_setservername(0, "pool.ntp.org");  // 144.76.59.37
        sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
        firstrun = false;
    }
    if (sntp_get_sync_status() == SNTP_SYNC_STATUS_COMPLETED)
        return true;
    else if (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET)
        sntp_init();
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < max_retries) {
        ESP_LOGD(TAG, "Waiting for system time to be set... (%d/%d)", retry, max_retries);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    if (retry == max_retries)
    {
        ESP_LOGE(TAG, "SNTP sync failed.");
        return false;
    }
    else return true;
}
