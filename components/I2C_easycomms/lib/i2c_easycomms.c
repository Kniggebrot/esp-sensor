#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

#include "esp_log.h"
#include "i2c_easycomms.h"
#include "sdkconfig.h"
#define I2C_PORT CONFIG_I2C_MASTER_PORT_NUM

#include <stdlib.h>

static bool init_success = false;
static SemaphoreHandle_t busy_bus = NULL;

static const char *TAG = "I2C";

esp_err_t init_i2c()
{
    if (init_success) return ESP_OK;
    esp_err_t err = ESP_OK;

    int i2c_master_port = I2C_PORT;       // Chooseable? 0 or 1
    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = CONFIG_I2C_MASTER_SDA,                  // GPIO pin at which data line is connected; make configurable?
        .sda_pullup_en = CONFIG_I2C_PULLUP_SDA,
        .scl_io_num = CONFIG_I2C_MASTER_SCL,                  // GPIO pin at which bus clock is connected; make configurable?
        .scl_pullup_en = CONFIG_I2C_PULLUP_SCL,
        .master.clk_speed = 400000,    // Speed (in Hz) for Fast Mode+ I2C
    };
    ESP_ERROR_CHECK( i2c_param_config(i2c_master_port, &conf) );
    err = i2c_driver_install(i2c_master_port, conf.mode, 0 , 0 , 0);
    busy_bus = xSemaphoreCreateMutex();
    assert(busy_bus);
    if (err == ESP_OK)  // don't "crash" app via ESP_ERROR_CHECK here, but only set init to successful if the driver install went well
    {
        ESP_LOGD(TAG, "Initialized I2C connection at line IO%d with clock IO%d in %s mode.", conf.sda_io_num, conf.scl_io_num, ( conf.mode == I2C_MODE_MASTER ? "MASTER" : "SLAVE") );
        init_success = true;
    }

    return err;
}

esp_err_t i2c_exec_cmd(i2c_cmd_handle_t *cmd)
{
    if (!init_success) init_i2c();

    uint8_t tens = 0;
    for (; ( xSemaphoreTake(busy_bus, 10 * 1000 / portTICK_PERIOD_MS) == pdFALSE ) && tens < 6; tens++)
    {
        ESP_LOGV(TAG, "Waiting for Semaphore to be available...");
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
    if (tens == 6)
    {
        ESP_LOGE(TAG, "Failed to obtain I2C semaphore for 60 s. Returning.");
        return ESP_ERR_TIMEOUT;
    }
    esp_err_t err = i2c_master_cmd_begin(I2C_PORT, *cmd, 1000 / portTICK_RATE_MS);
    xSemaphoreGive(busy_bus);

    return err;
    
}

inline void write_master(i2c_cmd_handle_t *cmd, uint8_t registeradr, uint8_t data)
{
    i2c_master_write_byte(*cmd, registeradr, WRITE_ACK);         // select register
    i2c_master_write_byte(*cmd, data, WRITE_ACK);                // write data to register
}                                                                // Multiple writes can happen without a stop inbetween.

void read_master(uint8_t registeradr, uint8_t *target, uint8_t deviceadr, uint8_t endadr)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();                                                // Read trimming values from chip
    i2c_master_start(cmd);
    
    i2c_master_write_byte(cmd, (deviceadr << 1) | I2C_MASTER_WRITE, WRITE_ACK);   // indicate WRITE to BME280 for register
    i2c_master_write_byte(cmd, registeradr, WRITE_ACK);         // select register
    i2c_master_start(cmd);                                                          // start, so sensor knows the following address is not data
    i2c_master_write_byte(cmd, (deviceadr << 1) | I2C_MASTER_READ, WRITE_ACK);    // indicate READ to BME280

    i2c_master_read(cmd, target, (endadr - registeradr) + 1, I2C_MASTER_LAST_NACK);

    i2c_master_stop(cmd);                                   // However, after each (burst) read, the sensor needs to know to stop. ITS TIME TO STOP
    ESP_ERROR_CHECK(i2c_exec_cmd(&cmd));
    i2c_cmd_link_delete(cmd);   
}

static void merge_bytes2words(uint8_t *input, uint16_t *output, uint8_t inlength)
{  
    uint8_t j = 0; // to count elements merged
    for (uint8_t i = 0; i < inlength; i+=2) // because 2 input entries are merged to 1 output
    {
        output[j] =  0;
        output[j] |= (uint16_t)input[i];              // usually values are stored in little endian, so first byte is put in first,
        output[j] |= ((uint16_t)( input[i+1] << 8) );       // and second byte for "upper" part; may need to write option for big endian?
        j++;
    }
}

void read_word(uint8_t registeradr, uint16_t *target, uint8_t deviceadr, uint8_t endadr)
{
    uint8_t elements = (endadr - registeradr) + 1;                    // how many bytes will be read?

    uint8_t *buffer = NULL;                                              // initialize buffer for reading bytes, to be merged if necessary
    buffer = calloc( elements, sizeof(uint8_t) );

    read_master(registeradr, buffer, deviceadr, endadr);
    merge_bytes2words(buffer, target, elements);
    free(buffer);
}

static void merge_bytes2words_s(uint8_t *input, int16_t *output, uint8_t inlength)
{
    uint8_t j = 0; // to count elements merged
    for (uint8_t i = 0; i < inlength; i+=2) // because 2 input entries are merged to 1 output
    {
        output[j] =  0;
        output[j] |= (uint16_t)input[i];              // usually values are stored in little endian, so first byte is put in first,
        output[j] |= ((uint16_t)(input[i+1] << 8));       // and second byte for "upper" part; may need to write option for big endian?
        j++;
    }
}

void read_word_s(uint8_t registeradr, int16_t *target, uint8_t deviceadr, uint8_t endadr)
{
    uint8_t elements = (endadr - registeradr) + 1;                    // how many bytes will be read?

    uint8_t *buffer = NULL;                                              // initialize buffer for reading bytes, to be merged if necessary
    buffer = calloc( elements, sizeof (uint8_t) );

    read_master(registeradr, buffer, deviceadr, endadr);
    merge_bytes2words_s(buffer, target, elements);
    free(buffer);
}