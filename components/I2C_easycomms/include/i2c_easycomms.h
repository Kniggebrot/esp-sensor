#ifndef I2C_easy_h
#define I2C_easy_h

#include "driver/i2c.h"

#define WRITE_ACK 0x1

esp_err_t init_i2c();                                                                                   // Initiate I2C bus with parameters from config

extern esp_err_t i2c_exec_cmd(i2c_cmd_handle_t *cmd);                                                   // Use to exec cmd with usage of mutex to avoid collision
extern void write_master(i2c_cmd_handle_t *cmd, uint8_t registeradr, uint8_t data);                     // "Space-saving" function: Combines writing of register and data in one command, but still needs the classic I2C cmd "frame" (with "i2c_master_start(cmd);" etc)
extern void read_master(uint8_t registeradr, uint8_t *target, uint8_t deviceadr, uint8_t endadr);       // reads a byte from registeradr on device (deviceadr) to target, and keeps reading bytes into bytes following target (hopefully already allocated) until endadr is reached. NEEDS NO FRAME
extern void read_word(uint8_t registeradr, uint16_t *target, uint8_t deviceadr, uint8_t endadr);        // reads unsigned words into 16-bit targets. NEEDS NO FRAME
extern void read_word_s(uint8_t registeradr, int16_t *target, uint8_t deviceadr, uint8_t endadr);       // reads signed words into 16-bit targets. NEEDS NO FRAME

#endif //I2C_easy_h