/*  Sensor - Main App

    The main app, calling read_data() to read temperature and pressure from sensor
    and send_data() to send the last 10 readings to the Raspberry Pi host.

    TODO: Allow configuration (via Wi-Fi AP) in browser, Wi-Fi options, measurement & send times, displayed name, mqtt options?
*/

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_spiffs.h"
#include "esp_idf_version.h"
#include "esp_pm.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp32/pm.h"
#include "esp_tls.h"
#include "driver/gpio.h"
#include "cJSON.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sdkconfig.h"

#include "BME280.h"
#include "SSD1306.h"
#include "sensorwifi.h"
#include "sensormesh.h"
#include "sensor_mqtt.h"

#define STATUSPIN GPIO_NUM_2

#define ADDRESS_BME CONFIG_ADDRESS_BME280  /* I2C address of sensor; for BME280 usually 0x76, may need to be detected */
#define ADDRESS_SSD CONFIG_ADDRESS_SSD1306 /* I2C address of display; either 0x3C or 0x3D, depending on config */

#define CTIME CONFIG_CTIME  /* Every ctime seconds data will be measured and collected (for testing 10, later maybe 60 or 120 s?), setting in menuconfig */

#ifdef CONFIG_CUSTOM_STIME  /* Start of check for STIME from menuconfig */
#define STIME CONFIG_STIME  /* Every stime seconds data will be send to Raspberry Pi (after 10 measurements) */
#else
#define STIME ( 10 * CONFIG_CTIME )
#endif /* USAGE OF CUSTOM STIME or just (10 * CTIME) */

#ifdef CONFIG_MESH_ENABLE /* Bool for WiFi mesh */
static const bool usemesh = true;
#else
static const bool usemesh = false;
#endif // Bool for WiFi mesh

static uint8_t coltime; //  = CTIME;
static uint16_t sendtime; // = STIME;
static char name[9];

bool *wifistatus = NULL;
static TaskHandle_t read_handle = NULL, send_handle = NULL, status_handle = NULL; // Task handles needed for notifications
TaskHandle_t mqtt_handle = NULL;
TaskHandle_t mqtt_conf_handle = NULL;

#ifdef CONFIG_SSD_USE_DISPLAY
static TaskHandle_t disp_handle = NULL;
static QueueHandle_t measurehdl = NULL;
#endif

static const char *TAG = "MAIN";
static const char *PATH= "/spiffs/measures.csv";
static const char *PATH2="/spiffs/measures_edit.csv";
static const char *csvstring= MACSTR",%ld,%d,%d,%d\n";

// CA certificate
extern const uint8_t ca_cert_start[] asm("_binary_ca_crt_start");
extern const uint8_t ca_cert_end[]   asm("_binary_ca_crt_end");

static bool sntp_success = false;
static time_t currenttime;
static time_t timediff = 0;
static SemaphoreHandle_t csv_lock = NULL;     // Mutex for csv
static uint8_t device_mac[6];

void read_data(uint8_t *coltime)
{    
    const char *TAG = "READ";
    measureptr firstmes = NULL, latestmes = NULL;
    bool csv_locked = false, firstread = true;                 // if doing a first read on I2C clock speed faster than 100000 (standard mode), it is usually garbage; so repeat immediately
    FILE *csvhandle = NULL;

    ESP_LOGD(TAG,"No first measurement allocated. Trying to allocate memory...");
    firstmes = (measureptr)calloc( 1,sizeof(measure) );
    assert(firstmes);
    latestmes = firstmes;

    TickType_t start = xTaskGetTickCount();

    ESP_LOGV(TAG, "Size of measureptr: %d bytes", sizeof(measureptr) );
    ESP_LOGV(TAG, "Size of ulValue: %d bytes", sizeof(uint32_t) );

    while (1)
    {
        ESP_LOGD(TAG,"Collecting data...");
        
        if (csv_locked)                                                                         // allocate new measurement memory if csv was locked
        {
            latestmes->next = (measureptr)calloc( 1,sizeof(measure) );
            latestmes = latestmes->next;
        }        
        
        currenttime = time(NULL);
        read_BME( ADDRESS_BME, latestmes);
        latestmes->timestamp = currenttime;

        ESP_LOGI(TAG, " \n\t Measured data: \n\t Temperature: %f C\n\t Pressure: %f hPa\n\t Humidity: %f %%", latestmes->temperature / 100.0f, latestmes->pressure / 10000.0f, latestmes->humidity / 1024.0f);
        if (!firstread)
        {
            // try to fill csv if mutex available, else continue
            if( xSemaphoreTake(csv_lock, 10 / portTICK_PERIOD_MS) == pdTRUE)          // Check if csv mutex is available. If not, try to paste into csv, else just continue
            {
                // paste into csv here, free pasted measurements (loops), set latest measurement to first and unset csv lock
                csvhandle = fopen(PATH,"a");
                fprintf(csvhandle,csvstring,MAC2STR(device_mac),firstmes->timestamp,firstmes->temperature,firstmes->pressure,firstmes->humidity);   // as firstmes will be ignored in the above loops, better print it seperately
                if (firstmes->next != NULL)
                {
                    while (firstmes->next != NULL)
                    {
                        latestmes = firstmes;
                        while (latestmes->next->next != NULL)
                        {
                            latestmes = latestmes->next;                    
                        }
                        fprintf(csvhandle,csvstring,MAC2STR(device_mac),latestmes->next->timestamp,latestmes->next->temperature,latestmes->next->pressure,latestmes->next->humidity);
                        free(latestmes->next);
                        latestmes->next = NULL;
                    }
                }
                latestmes = firstmes;
                fclose(csvhandle);
                xSemaphoreGive(csv_lock);                                           // Since were done, give semaphore for send (and itself)  
                csv_locked = false;
                
                ESP_LOGD(TAG,"Pasted measurements into csv.");
            }
            else
            {
                ESP_LOGD(TAG,"Couldn't open csv as it is currently blocked. Will try to write on next read");
                csv_locked = true;
            }
            // else it will just continue, adding measurements to the chain because csv_locked is true                                 
            // output measured data to display
#ifdef CONFIG_SSD_USE_DISPLAY 

            if (disp_handle) xTaskNotify(disp_handle, (uint32_t)latestmes, eSetValueWithoutOverwrite);               // casting address to uint32, as it is 32bit as well; needs to be "uncasted" in displaynow!           
            else   // Try to create a queue for this item, so it can be received by disp_handle as soon as it's started
            {
                for (uint8_t tries = 0; (measurehdl == NULL) && tries < 10; tries++)
                {
                    ESP_LOGV(TAG, "Allocating measurement queue");
                    measurehdl = xQueueCreate(1, sizeof(measureptr));
                    vTaskDelay(100 / portTICK_PERIOD_MS);
                }
                if (measurehdl) xQueueSend(measurehdl, &latestmes, 1000 / portTICK_PERIOD_MS);
            }
#endif
            vTaskDelayUntil( &start, ( (*coltime * 1000) / portTICK_PERIOD_MS) );                                      // wait until next read
        }

        else firstread = false;                                           // if firstread: just set firstread to false and do another read
        

    }
    vTaskDelete(NULL);    
}

void skip_line(FILE **stream)   // Skip to char after next newline char
{
    int test = fgetc(*stream);
    while (test != '\n' && test != EOF)
        test = fgetc(*stream);
}

void rewind_line(FILE **stream) // Go back to start of line (after newline char)
{
    char test = fgetc(*stream);
    while (test != '\n' && ftell(*stream) > 1)
    {
        fseek(*stream, -2, SEEK_CUR);
        test = fgetc(*stream);
    }
    if (ftell(*stream) == 1 && test != '\n')
    {
        fseek(*stream, -1, SEEK_CUR);
        return;
    }
    
}

uint64_t get_filesize(FILE **stream) // Get rest of characters in a stream until EOF; filesize if done at the beginning
{
    uint64_t length = 0;
    long current = ftell(*stream);

    fseek(*stream, 0, SEEK_END);
    length = ftell(*stream);

    // set stream to previous position;
    fseek(*stream, current, SEEK_SET);
    return (length - current);
}

void send_data(uint16_t *sendtime)
{
    // If finding "too old" measurements, recheck time and calculate real time from time since startup with esp_timer_get_timer() from esp_timer.h
    const char *TAG = "SEND";
    FILE *csvhandle = NULL, *csvedit = NULL;
    measureptr item = NULL;
    TickType_t start = xTaskGetTickCount();

    while (1)
    {
        ESP_LOGV(TAG, "Send task begins.");
        vTaskDelayUntil( &start,( (*sendtime * 1000) / portTICK_PERIOD_MS ) );

        while ( !(*wifistatus) )      
        {
            ESP_LOGD(TAG, "Waiting for successful WiFi connection...");
            vTaskDelay(5000 / portTICK_PERIOD_MS);
            
        }
        ESP_LOGV(TAG, "Got wifi.");
        
        while( (xSemaphoreTake(csv_lock, ( (10 * 1000) / portTICK_PERIOD_MS) )) == pdFALSE)     // Try to obtain the csv semaphore for 10 s. If still blocked, repeat after 10 ms
        {
            ESP_LOGD(TAG, "Waiting for csv mutex to become availiable...");
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        ESP_LOGV(TAG, "Got csv.");

        item = (measureptr)calloc( 1, sizeof(measure) );
        assert(item);
        ESP_LOGV(TAG, "Asserted item.");

        csvhandle = fopen(PATH,"r");
        csvedit = fopen(PATH2, "a");                    // May lead to items being sent twice, but better than losing all previous items
        ESP_LOGV(TAG, "Opened files.");

        // Looping through csv to find wrong items, writing corrected (or -able) items to edit file
        for (int matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity); matched != EOF; matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity) )
        {            
            // First loop: checking csv for measurements from older app versions...
            // Matched is most likely going to be one, since the first 2 digit hex will always be matched; or zero if oldest firmware + a measurement close to 0 degrees, which will be dumped anyway
            // Oldest version: Only 3 elements (temp, humi and pres)
            if (matched != 10)
            {
                float tempbuf, presbuf, humibuf;
                if (matched == 8)
                {
                    // "Old" type with new id and timestamp, but float instead of ints
                    ESP_LOGW(TAG, "Measurement with device id and timestamp, but floats instead of integers found. Rescanning...");
                    rewind_line(&csvhandle);
                    matched = fscanf(csvhandle, MACSTR""",%ld,%f,%f,%f", MAC2SSTR(item->device_id), &item->timestamp, &tempbuf, &presbuf, &humibuf);
                    // Not checking id, since it's a new usable one anyway
                    if (matched == 10)
                    {
                        item->temperature   = tempbuf * 100;
                        item->pressure      = presbuf * 10000;
                        item->humidity      = humibuf * 1024;
                    }
                    else goto Unreadable;
                }
                else
                {
Unreadable:
                    ESP_LOGE(TAG, "ERROR: Not all measurement components could be matched to values for the csv.");
                    ESP_LOGE(TAG, "Measurement with %d matched arguments found, leading to:", matched);
                    ESP_LOGE(TAG, "Measured by device "MACSTR, MAC2STR(item->device_id));
                    ESP_LOGE(TAG, " \n\t Measured on: %s \n\t Temperature: %.02f C\n\t Pressure: %.02f hPa\n\t Humidity: %.02f %%", ctime(&item->timestamp), item->temperature / 100.0f, item->pressure / 10000.0f, item->humidity / 1024.0f);
                    ESP_LOGE(TAG, "Skipping this measurement.");
                    skip_line(&csvhandle);
                    // Clean item
                    memset(item, 0, sizeof(*item));
                    continue;
                }
            }
            if (item->timestamp < 1262304000)        // measurement before 1.1.2010?! 
            {
                ESP_LOGW(TAG, "Measurement before timesync found; assuming, it has been made after the last reboot...");
                uint8_t retries = 0;
                while (!timediff && (retries < 10) )                    // if connection is very recent, time may not have been synced. So wait a little...
                {
                    timediff = time(NULL) - (esp_timer_get_time() / 1000000);
                    vTaskDelay(1001 / portTICK_PERIOD_MS);
                    retries++;
                }
                if (!timediff)
                {
                    ESP_LOGE(TAG, "Couldn't sync time after 10 retries.");
                    // but won't drop it, as we can try to sync later
                }
                else
                {
                    item->timestamp += timediff;
                }
                if (item->timestamp > time(NULL))
                {
                    ESP_LOGD(TAG, "A measurement from the future?");
                    // item->timestamp = time(NULL);                        // Not accurate, but oh well...
                    // Clean item
                    memset(item, 0, sizeof(*item));
                    continue;                                               // Better skip it; but no need to skip line, since it has been fully read already
                }
            }
            // Write measurement to edit-csv
            ESP_LOGD(TAG, "Writing measurement back to edit csv...");                
            fprintf(csvedit, csvstring, MAC2STR(item->device_id), item->timestamp, item->temperature, item->pressure, item->humidity );  // write corrected line to edit file;

            // Reallocate item, to ensure it's clean
            // Clean item
            memset(item, 0, sizeof(*item));
        }
        fclose(csvedit);
        fclose(csvhandle);
        remove(PATH);
        xSemaphoreGive(csv_lock);                                                   // allow read to access the "dirty" csv again

        xTaskNotify(mqtt_handle, 1, eSetValueWithoutOverwrite);        // Don't care if status send fails, since the measurements in the current csv should be "cleaned" anyway
        // Variables for sending one item at a time
        csvhandle = fopen(PATH2, "r");  // now csvhandle accesses the "clean" csv!
        for (int matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity); matched != EOF; matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity) )
        {
            ESP_LOGD(TAG, "Reading \"clean\" csv..."); // now from csvhandle
                      
            // if item wasn't synced, try again or rewrite to "dirty" csv            
            if (item->timestamp < 1262304000)        
            {
                // ESP_LOGW(TAG, "Measurement before timesync found; assuming, it has been made after the last reboot...");
                uint8_t retries = 0;
                while (!timediff && (retries < 10) )                    // if connection is very recent, time may not have been synced. So wait a little...
                {
                    timediff = time(NULL) - (esp_timer_get_time() / 1000000);
                    vTaskDelay(1001 / portTICK_PERIOD_MS);
                    retries++;
                }
                if (!timediff)
                {
                    ESP_LOGE(TAG, "Couldn't sync time after 10 retries.");
                    // Write to "dirty" csv
                    while( (xSemaphoreTake(csv_lock, ( (10 * 1000) / portTICK_PERIOD_MS) )) == pdFALSE)     // Try to obtain the csv semaphore for 10 s. If still blocked, repeat after 10 ms
                    {
                        ESP_LOGD(TAG, "Waiting for csv mutex to become availiable...");
                        vTaskDelay(10 / portTICK_PERIOD_MS);
                    }
                    csvedit = fopen(PATH, "a");
                    fprintf(csvedit, csvstring, MAC2STR(item->device_id), item->timestamp, item->temperature, item->pressure, item->humidity);
                    fclose(csvedit);
                    xSemaphoreGive(csv_lock);
                    // Clean item
                    memset(item, 0, sizeof(*item));
                    continue;                                               // make sure items with wrong timestamp are skipped

                }
                else
                {
                    item->timestamp += timediff;
                }
                if (item->timestamp > time(NULL))
                {
                    ESP_LOGD(TAG, "A measurement from the future?");
                    // item->timestamp = time(NULL);                        // Not accurate, but oh well...
                    // Clean item
                    memset(item, 0, sizeof(*item));
                    continue;                                               // make sure items with wrong timestamp are skipped
                }
                
            }
            
            
            // for now, print out all current measurement entries
            ESP_LOGW(TAG, "Send attempt:");
            ESP_LOGD(TAG, "Measured by device "MACSTR, MAC2STR(item->device_id));
            ESP_LOGW(TAG, " \n\t Measured on: %s \n\t Temperature: %.02f C\n\t Pressure: %.02f hPa\n\t Humidity: %.02f %%", ctime(&item->timestamp), item->temperature / 100.0f, item->pressure / 10000.0f, item->humidity / 1024.0f);
            // Send item
            esp_err_t err = send_measure(item);
            if (err != ESP_OK) // Andere Fehler ergänzen?
            {
                ESP_LOGE(TAG, "Unable to connect to host. Pasting measurements back to original csv...");
                char *buffer = NULL;
                rewind_line(&csvhandle);
                // write measurements from edit csv back to standard measures csv
                while( (xSemaphoreTake(csv_lock, portMAX_DELAY) == pdFALSE) )    // Try to obtain the csv semaphore forever (until obtained).
                {
                    ESP_LOGD(TAG, "Waiting for csv mutex to become availiable...");
                    vTaskDelay(10 / portTICK_PERIOD_MS);
                }
                csvedit = fopen(PATH, "a");
                for (int matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity); matched != EOF; matched = fscanf(csvhandle, csvstring, MAC2SSTR(item->device_id), &item->timestamp, &item->temperature, &item->pressure, &item->humidity) ) {
                    fprintf(csvedit, csvstring, MAC2STR(item->device_id), item->timestamp, item->temperature, item->pressure, item->humidity);
                    // Clean item
                    memset(item, 0, sizeof(*item));
                }
                
                // close file, break loop
                fclose(csvedit);
                xSemaphoreGive(csv_lock);
                free(buffer);
                break;
            }
            
            
            // Clean item
            memset(item, 0, sizeof(*item));
            assert(item);

        }
        fclose(csvhandle);
        ESP_LOGW(TAG,"Finished read of csv.");
        // close mqtt connection
        // ESP_ERROR_CHECK(set_conn(false));
        // Clear measurements
        remove(PATH2);
        free(item);
    }
    
    vTaskDelete(NULL);
    
}

const char* reset_reason(esp_reset_reason_t lastreset);

typedef struct {
    char name[9];
    char lastreset[19];
} statussetup;
static statussetup setup;

void status_prep(void)
{
    uint32_t full = 0;
    statusmsg status;
    #if CONFIG_USE_CUSTOM_NAME
    strncpy(status.name, setup.name, 9 * sizeof(char));
    #endif

    strncpy(status.lastreset, setup.lastreset, 19 * sizeof(char));
    memcpy(status.mac, device_mac, 6 * sizeof(uint8_t) );

    while(1)
    {
        xTaskNotifyWait(0, 0xffffffffUL, &full, portMAX_DELAY);
        if(full >= 2)
        {
            send_fullstat(&status);
        }
        else
        {
            send_status(&status);
        }
    }
    vTaskDelete(NULL);    
}

#ifdef CONFIG_SSD_USE_DISPLAY   // Task for initializing display and outputting measurements
void displayme()
{
    uint32_t message;
    measureptr latest = NULL;
    char temp[8], humi[8], pres[11]; // Operating ranges of BME280: -40...85C, 0...100%, 300...1100 hPa

    for (uint8_t tries = 0; (measurehdl == NULL) && (tries < 10); tries++)
    {
        ESP_LOGV(TAG, "Waiting for measurement queue...");
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
    if (measurehdl != NULL)
    {
        uint8_t tries;
        for (tries = 0; (xQueueReceive(measurehdl, &message, 1000 / portTICK_PERIOD_MS) != pdTRUE) && (tries < 10); tries++)
        {
            ESP_LOGV(TAG, "Queue recv %d", tries);
        }
        if (tries < 10)
        {
            xTaskNotify(xTaskGetCurrentTaskHandle(), message, eSetValueWithoutOverwrite);
        }
        vQueueDelete(measurehdl);
        vTaskDelay(500 / portTICK_PERIOD_MS); // for no good reason
    }
    
    while (1)
    {
        xTaskNotifyWait(0,0xffffffffUL,&message,portMAX_DELAY);          // Wait until read has send a measurement index to be displayed
        latest = (measureptr)message;
        ESP_LOGV(TAG, "Latest points to: %p", latest);
        assert(latest);
        sprintf(temp,"%.1f C", (latest->temperature / 100.0f) );
        sprintf(humi,"%.1f %%", (latest->humidity / 1024.0f) );
        sprintf(pres,"%.1f hPa", (latest->pressure / 10000.0f) );

        ESP_LOGV(TAG,"Sending to Display:\n\t%s %s %s", temp, humi, pres);
        ESP_ERROR_CHECK( display_measurement(ADDRESS_SSD, temp, humi, pres) );

    }
    vTaskDelete(NULL);
    
}

void displaystatus()
{
    wifi_ap_record_t wifi_status;
    const uint8_t refresh = CONFIG_SSD_STATUS_REFRESH;           // set in menuconfig for faster / slower status refreshing
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);     // simply wait for display to be initiated
    while (1)
    {
        if ( wifistatus != NULL && (*wifistatus))
        {
            esp_wifi_sta_get_ap_info(&wifi_status);
            display_status(ADDRESS_SSD, usemesh, &device_mac[4], name, wifi_status.rssi);
        }
        else
        {
            display_status(ADDRESS_SSD, false, &device_mac[4], name, (-101));              // don't want to try to find a parent if disconnected
        }
        if ( time(NULL) % refresh)
        {
            ESP_LOGV(TAG,"Waiting short...");
            ulTaskNotifyTake(pdTRUE, (refresh - (time(NULL) % refresh)) * 1000 / portTICK_PERIOD_MS);
        }
        else 
        {
            ESP_LOGV(TAG,"Waiting long.");
            ulTaskNotifyTake(pdTRUE, refresh * 1000 / portTICK_PERIOD_MS);
        }
        
    }
    vTaskDelete(NULL);
}

void init_display()
{
    const uint8_t address = ADDRESS_SSD;
    ESP_ERROR_CHECK( init_disp(address) );
    // Start tasks for display
    xTaskCreate((TaskFunction_t)displaystatus, "DISPTIME", 2048, NULL, 6, &status_handle);
    xTaskCreate((TaskFunction_t)displayme, "DISP_MES", 3072, NULL, 6, &disp_handle);
    xTaskNotifyGive(status_handle);     // Let status know that init is done, so it can start displaying the status
    vTaskDelete(NULL);

}
#endif // Tasks for display

esp_err_t init_spiffs() // from SPIFFS example in esp-idf
{
    const char *TAG = "SPIFFS";

    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = true
    };
    
    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return ret;
    }
    
    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d (%3.2f %%)", total, used, ( ((float)used / (float)total) * 100) );
    }

    ESP_LOGD(TAG, "Testing SPIFFS partition...");
    const char teststring[] = "Hello SPIFFS!";
    // write file
    ESP_LOGD(TAG, "Opening file");
    FILE* f = fopen("/spiffs/hello.txt", "w");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        return ret;
    }
    fprintf(f, teststring);
    fclose(f);
    ESP_LOGD(TAG, "File written");
    // read file
    ESP_LOGD(TAG, "Reading file");
    f = fopen("/spiffs/hello.txt", "r");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file for reading");
        return ret;
    }
    char line[64];
    fgets(line, sizeof(line), f);
    fclose(f);
    // now compare
    ret = strcmp(teststring,line);
    if(ret) 
    {
        ESP_LOGE(TAG,"Something failed while reading or writing the testring %s to the file;\n\tit became %s", teststring, line);
        ret = ESP_FAIL;     // if strings are not equal, return ESP_FAIL
    }
    else
    {
        ESP_LOGI(TAG,"SPIFFS init successful.");
        remove("/spiffs/hello.txt");
        ret = ESP_OK;
    }

    return ret;
}

const char* reset_reason(esp_reset_reason_t lastreset)
{
    switch (lastreset)
    {
        case ESP_RST_POWERON:
            return "ESP_RST_POWERON"; // (Reset by power-on event; usually after flashing app)";
        case ESP_RST_EXT:
            return "ESP_RST_EXT"; // (Reset by external pin; impossible?!)";
        case ESP_RST_SW:
            return "ESP_RST_SW"; // (Reset by software, e.g. esp_restart(), shouldn't happen...)";
        case ESP_RST_PANIC:
            return "ESP_RST_PANIC"; // (Reset by exception/panic in app, check app now)";
        case ESP_RST_INT_WDT:
            return "ESP_RST_INT_WDT"; // (Reset by interrupt watchdog, maybe a problem with the app?)";
        case ESP_RST_TASK_WDT:
            return "ESP_RST_TASK_WDT"; // (Reset by task watchdog, maybe a problem with the app?)";
        case ESP_RST_WDT:
            return "ESP_RST_WDT"; // (Reset by a watchdog; also used if reset by reset button on board)";
        case ESP_RST_DEEPSLEEP:
            return "ESP_RST_DEEPSLEEP"; // (Reset after exiting deep sleep)";
        case ESP_RST_BROWNOUT:
            return "ESP_RST_BROWNOUT"; // (Reset due to brownout)";
        case ESP_RST_SDIO:
            return "ESP_RST_SDIO"; // (Reset over SDIO)";
        default:
            return "ESP_RST_UNKNOWN"; // (Unknown last reset reason)";        
    }
}

void classic_wifistatus(EventGroupHandle_t **wifi_events)
{
    EventBits_t wifibits;
    while (1)
    {
        wifibits = xEventGroupWaitBits(**wifi_events, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdFALSE, pdFALSE, 10 / portTICK_PERIOD_MS);
        if ( (wifibits & WIFI_FAIL_BIT) || !(wifibits & WIFI_CONNECTED_BIT) )
        {
            *wifistatus = false;
            esp_wifi_connect();
        }
        else
            *wifistatus = true;
        vTaskDelay(5000 / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

bool *get_wifistatus(bool ismesh)
{
    if (ismesh)
        return get_mesh_conn();
    else
    {
        // Using classic WiFi
        static EventGroupHandle_t *wifi_events = NULL;
        wifi_events = get_wifi_events();
        wifistatus = (bool*)calloc(1,sizeof(bool));
        assert(wifistatus);
        xTaskCreate((TaskFunction_t)classic_wifistatus, "WIFIWATCH", 1024, (void *)&wifi_events, 10, NULL);
        return wifistatus;
    }
}

void timesync()
{
    const char *TAG = "TIME";
    vTaskDelay(5001 / portTICK_PERIOD_MS);
    while (!sntp_success)
    {
        
        while (!(*wifistatus))
        {
            ESP_LOGD(TAG, "Waiting for connection to WiFi...");
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
        if (status_handle) xTaskNotifyGive(status_handle);
        ESP_LOGW(TAG, "Connected. Syncing time via SNTP...");
        sntp_success = start_timesync();
        
        
    }
    currenttime = time(NULL);
    ESP_LOGI(TAG, "Current Time: %s", ctime(&currenttime));
    vTaskDelete(NULL);    
}

void startup()
{
    esp_pm_config_esp32_t pmconfig = {
        .max_freq_mhz = 160,
        .min_freq_mhz = CONFIG_ESP32_XTAL_FREQ,
        .light_sleep_enable = true
    };
    ESP_ERROR_CHECK(esp_pm_configure(&pmconfig));
    // Use pin 2 as possible "status" led; long blink on startup, short blink on send
    gpio_config_t statusled = {
        .pin_bit_mask = (1ULL << STATUSPIN),
        .mode = GPIO_MODE_OUTPUT,
    };
    ESP_ERROR_CHECK(gpio_config(&statusled));
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 1));
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 0));
    vTaskDelete(NULL);
}

/* TODO:
 * Config via MQTT:
 * - On first boot, write default config options from KConfig to NVS
 * - Set up subscription for MQTT channel
 * - When new config option is received, validate, write option to file and restart (after send and read complete; check their statuses, suspend send and assure all reads have been saved to csv, then take semaphore and suspend it as well)
 * - On restart, read new options and write to NVS
 * - Option invalid? Restore default config
 * - If first boot after flashing (detect via file on spiffs?), restore config to macro values
 */
esp_err_t config_to_nvs()
{
    esp_err_t err;
    nvs_handle_t rwhandle;
    size_t str_len;

    ESP_ERROR_CHECK(nvs_open("CONFIG", NVS_READWRITE, &rwhandle));

    FILE *resetcfg = NULL;
    resetcfg = fopen("/spiffs/reset_cfg","r");
    if (resetcfg) {
        // Clear nvs config namespace
        ESP_LOGW(TAG, "Restoring default config...");
        nvs_erase_all(rwhandle);
        ESP_ERROR_CHECK(nvs_commit(rwhandle));
        ESP_LOGW(TAG, "Cleared config nvs successfully.");
        fclose(resetcfg);
        resetcfg = NULL;
        remove("/spiffs/reset_cfg");
    }
    fclose(resetcfg);

    err = nvs_get_u8(rwhandle, "CTIME", &coltime);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "CTIME not found in NVS. Writing it to it... (err: %s)", esp_err_to_name(err));
        nvs_set_u8(rwhandle, "CTIME", CTIME);
        ESP_ERROR_CHECK(nvs_commit(rwhandle));
        err = nvs_get_u8(rwhandle, "CTIME", &coltime);
        ESP_LOGE(TAG, "CTIME read: %s", esp_err_to_name(err));
    }
    err = nvs_get_u16(rwhandle, "STIME", &sendtime);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "STIME not found in NVS. Writing it to it... (err: %s)", esp_err_to_name(err));
        nvs_set_u16(rwhandle, "STIME", STIME);
        ESP_ERROR_CHECK(nvs_commit(rwhandle));
        err = nvs_get_u16(rwhandle, "STIME", &sendtime);
        ESP_LOGE(TAG, "STIME read: %s", esp_err_to_name(err));
    }
    nvs_get_str(rwhandle, "NAME", NULL, &str_len);
    err = nvs_get_str(rwhandle, "NAME", name, &str_len);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "NAME not found in NVS. Writing it to it... (err: %s)", esp_err_to_name(err));
        nvs_set_str(rwhandle, "NAME", CONFIG_CUSTOM_NAME);
        ESP_ERROR_CHECK(nvs_commit(rwhandle));
        nvs_get_str(rwhandle, "NAME", NULL, &str_len);
        err = nvs_get_str(rwhandle, "NAME", name, &str_len);
        ESP_LOGE(TAG, "NAME read: %s", esp_err_to_name(err));
    }
    ESP_LOGW(TAG, "CTIME: %d s, STIME: %d s, Name: %s", coltime, sendtime, name);
    ESP_LOGW(TAG, "NVS readout done.");
    nvs_close(rwhandle);
    return err;
}

void mqtt_conf_task(void)
{
    esp_err_t err;
    char *config_str = NULL;

    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    do {
        err = send_config(coltime, sendtime, name, device_mac);
    } while (err != ESP_OK);
    do {
        err = init_config_listen(device_mac);
    } while (err != ESP_OK);

    while (1)
    {
        // Notification received? Set config and restart ESP when send task is sleeping
        config_str = (char *)ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        if (!config_str) continue; // Skip config read if string is nonexistent
        cJSON *json = NULL;
        json = cJSON_Parse(config_str);
        if (json) {
            // Should have 3 components: CTIME, STIME and NAME
            const cJSON *ctime, *stime, *name;
            // readback
            u8_t ctime_read;
            u16_t stime_read;
            char name_read[9];

            ctime = cJSON_GetObjectItem(json, "CTIME");
            stime = cJSON_GetObjectItem(json, "STIME");
            name  = cJSON_GetObjectItem(json, "NAME");

            nvs_handle_t remote_write;
            ESP_ERROR_CHECK(nvs_open("CONFIG", NVS_READWRITE, &remote_write));

            if (cJSON_IsNumber(ctime) && (ctime->valueint < 255) )
            {
                nvs_set_u8(remote_write, "CTIME", (u8_t)ctime->valueint);
                ESP_ERROR_CHECK(nvs_commit(remote_write));
                err = nvs_get_u8(remote_write, "CTIME", &ctime_read);
                ESP_LOGE(TAG, "CTIME read: %d, %s", ctime_read, ctime_read == (u8_t)ctime->valueint ? "OK" : "FAILED");
            }
            if (cJSON_IsNumber(stime) && (stime->valueint < 32000) ) // It's an int; higher values would be impractical
            {
                nvs_set_u16(remote_write, "STIME", (u16_t)stime->valueint);
                ESP_ERROR_CHECK(nvs_commit(remote_write));
                err = nvs_get_u16(remote_write, "STIME", &stime_read);
                ESP_LOGE(TAG, "CTIME read: %d, %s", stime_read, stime_read == (u8_t)stime->valueint ? "OK" : "FAILED");
            }
            if (cJSON_IsString(name) && (name->string != NULL) && (strlen(name->string) < 8) )
            {
                size_t str_len;
                nvs_set_str(remote_write, "NAME", name->string);
                ESP_ERROR_CHECK(nvs_commit(remote_write));
                err = nvs_get_str(remote_write, "NAME", NULL, &str_len);
                err = nvs_get_str(remote_write, "NAME", name_read, &str_len);
                ESP_LOGE(TAG, "CTIME read: %s, %s", name_read, (strcmp(name_read, name->string) == 0) ? "OK" : "FAILED");
            }
            nvs_close(remote_write);
            cJSON_Delete(json);

            // Now, ensure send is done by seizing the csv semaphore and reboot
            while( (xSemaphoreTake(csv_lock, ( (10 * 1000) / portTICK_PERIOD_MS) )) == pdFALSE)     // Try to obtain the csv semaphore for 10 s. If still blocked, repeat after 10 ms
            {
                ESP_LOGD(TAG, "Waiting for csv mutex to become availiable...");
                vTaskDelay(10 / portTICK_PERIOD_MS);
            }
            vTaskDelay(500 / portTICK_PERIOD_MS);
            esp_restart(); // And off we go!
        }
        else {
            ESP_LOGE(TAG, "Received invalid json config. Skipped.");
            const char *error = cJSON_GetErrorPtr();
            if (error) {
                ESP_LOGE(TAG, "Failed config string: %s", config_str);
                ESP_LOGE(TAG, "Parsing error happened before %s", error);
            }
        }
        
    }

    vTaskDelete(NULL);
}

void app_main()
{

    ESP_LOGW(TAG, "Starting ESP-Sensor application... (built with IDF version %s)", esp_get_idf_version());
    ESP_LOGW(TAG, "Last reset due to %s", reset_reason( esp_reset_reason() ));
    // Power saving; fürs erste nur light sleep, später auch bei nicht-mesh in den deep sleep!
    xTaskCreate((TaskFunction_t)startup, "GPIO_START", 2048, NULL, 6, NULL);
    // Initialize NVS for WiFi, config
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_LOGW(TAG, "Initializing SPIFFS partition...");
    ESP_ERROR_CHECK( init_spiffs() );

    ESP_LOGW(TAG, "Checking NVS for readout settings...");
    ESP_ERROR_CHECK( config_to_nvs() );

#ifdef CONFIG_SSD_USE_DISPLAY
    {
        ESP_LOGW(TAG, "Initializing Display SSD1306...");
        xTaskCreate((TaskFunction_t)init_display, "SSDINIT", 2048, NULL, 5, NULL);
    }
#endif

    ESP_ERROR_CHECK(esp_read_mac(device_mac, 0));           // MAC needed for measurements; if wanting to set a custom MAC, please do it before here!
    // Populate global CA store with our CA, so it can be used for MQTT
    ESP_ERROR_CHECK(esp_tls_set_global_ca_store(ca_cert_start, ca_cert_end - ca_cert_start) );
    // Init status send & config task
    strncpy(setup.lastreset, reset_reason( esp_reset_reason() ), 19 * sizeof(char));
    strncpy(setup.name, name, 9 * sizeof(char));
    xTaskCreate((TaskFunction_t)status_prep, "STATUS_PREP", 3096, &setup, 9, &mqtt_handle);
    xTaskCreate((TaskFunction_t)mqtt_conf_task, "CONFIG_MQTT", 3096, NULL, 9, &mqtt_conf_handle);

    ESP_LOGW(TAG, "Initializing Sensor BME280...");
    ESP_ERROR_CHECK( init_BME(ADDRESS_BME) );

    csv_lock = xSemaphoreCreateMutex();
    ESP_LOGI(TAG,"Starting measurements...");
    xTaskCreate((TaskFunction_t)read_data, "READ_BME", 4096, (void *)&coltime, 7, &read_handle);
    xTaskCreate((TaskFunction_t)send_data, "SEND_BME", 3072, (void *)&sendtime, 8, &send_handle);

    ESP_LOGW(TAG, "Connecting to WiFi %s...", ( usemesh ? "mesh" : "AP") );
    if (usemesh)
    {
        esp_log_level_set("mesh", ESP_LOG_WARN);                // prevent mesh spam; comment out if trying to debug
        ESP_ERROR_CHECK( init_mesh() );
    }
    else
        ESP_ERROR_CHECK( init_wifi() );
    wifistatus = get_wifistatus(usemesh);
    assert(wifistatus);
    for (uint16_t retries = 0; !(*wifistatus) && (retries < 12*100); retries++)
    {
        vTaskDelay(10 / portTICK_PERIOD_MS);    // to give Wifiwatch time to update the connection status, and the ESP (especially in a mesh) time to connect
    }
    if ( *wifistatus )
    {
        if (status_handle) // status_handle cannot be NULL
            xTaskNotifyGive(status_handle);
        ESP_LOGW(TAG, "Connected. Syncing time via SNTP...");
        sntp_success = start_timesync();
        currenttime = time(NULL);
        ESP_LOGI(TAG, "Current Time: %s", ctime(&currenttime));
        timediff = time(NULL) - (esp_timer_get_time() / 1000000);
    }
    else
    {
        ESP_LOGE(TAG, "Unable to sync time, as WiFi seems to be disconnected.");
        ESP_LOGD(TAG,"Connected: %d, Fail: %d", ( *wifistatus ? 1 : 0), ( !(*wifistatus) ? 1: 0));
        xTaskCreate((TaskFunction_t)timesync, "TIMESYNC", 2048, NULL, 6, NULL);
    }
    ESP_LOGW(TAG,"Last 2 bytes of WiFi MAC: %02x:%02x", device_mac[4], device_mac[5]);

    ESP_LOGW(TAG, "Completed startup. Will attempt to collect data every %d seconds and send every %d seconds.", coltime, sendtime);

    // blink twice
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 1));
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 0));
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 1));
    vTaskDelay(100 / portTICK_PERIOD_MS);
    ESP_ERROR_CHECK(gpio_set_level(STATUSPIN, 0));
    ESP_ERROR_CHECK(init_mqtt());

#if ( LOG_LOCAL_LEVEL > 3 ) /* Log level for info; bigger means standard log lvl is debug or greater */
    while (1)
    {
        
        // esp_pm_dump_locks(stdout); // Power management locks, dumped to stdout. Uncomment to see how long ESP has been held in "max power" mode by what task
        
        if ( LOG_LOCAL_LEVEL > ESP_LOG_INFO )
        {
            ESP_LOGI(TAG, "\nAvailable heap size: %d bytes\n", esp_get_free_heap_size());
            char buffer[1024];
            vTaskList(buffer);
            printf("Name\t\tState\tPrio\tStack\tNum\tCore\n");
            printf("****************************************************\n");
            printf(buffer);
            printf("\n");
        }  
    
        vTaskDelay(300000 / portTICK_PERIOD_MS);
    }
#endif

}
