#include "esp_log.h"
#include "esp_wifi.h"
#include "mqtt_client.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "soc/adc_channel.h"
#include "esp_adc_cal.h"

#include "sensor_mqtt.h"

#define ADC_IN(NUM) ( ADC1_GPIO ## NUM ## _CHANNEL )
#define ADC_CH(NUM) ADC_IN(NUM)

extern uint8_t temprature_sens_read(); // For reading internal temperature sensor (bit wonky)

extern const char clientcert_start[] asm("_binary_client_crt_start");
extern const char clientcert_end[] asm("_binary_client_crt_end");

extern const char clientkey_start[] asm("_binary_client_key_start");
extern const char clientkey_end[] asm("_binary_client_key_end");

static const char *TAG = "MQTT";
static bool noinit = true;
static bool connected = false;
static esp_mqtt_client_handle_t client = NULL;

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);

esp_err_t init_mqtt(void)
{
    esp_err_t err = ESP_OK;

    if (noinit)
    {
        esp_mqtt_client_config_t conf = {
            .uri = CONFIG_MQTT_URI,
#ifdef CONFIG_STIME
            .keepalive = CONFIG_STIME + 60,
#else
        	.keepalive = 10 * CONFIG_CTIME + 60,
#endif
            .event_loop_handle = mqtt_event_handler,
            .use_global_ca_store = true,
            .client_cert_pem = clientcert_start,
            .client_key_pem = clientkey_start,
        };

        if (conf.keepalive < 120) conf.keepalive = 120;

        esp_err_t err = esp_event_loop_create_default();
        if (err != ESP_OK)
        {
            if (err == ESP_ERR_INVALID_STATE) {
                ESP_LOGV(TAG, "Default event loop already created. Ok...");
            }
            else {
                ESP_ERROR_CHECK(err);
            }
        }
        client = esp_mqtt_client_init(&conf);
        assert(client);
        esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
        err = esp_mqtt_client_start(client);
        if (err == ESP_OK)
        {
            noinit = false;
        }

    }
    return err;
}

static esp_err_t set_conn(bool goconnected)
{
    esp_err_t err;
    if (noinit) init_mqtt();
    if (!goconnected)
    {
        err = esp_mqtt_client_disconnect(client);
        // err = esp_mqtt_client_stop(client);
    }
    else
    {
        err = (esp_event_post("MQTT_EVENTS", MQTT_EVENT_BEFORE_CONNECT, NULL, sizeof(NULL), portMAX_DELAY));
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    return err;    
}

static bool adcinit = false;
static esp_adc_cal_characteristics_t calib_info;

esp_err_t init_adc(void)
{
    esp_err_t err = ESP_OK;

    if (adcinit) return ESP_OK;
    else
    {
        ESP_ERROR_CHECK(adc1_config_width(ADC_WIDTH_BIT_12));
        // Get channel for GPIO pin with VOLTAGE DIVIDED Vin (divided voltage has to be lower than 3.3V !!!!!)
        ESP_ERROR_CHECK(adc1_config_channel_atten(ADC_CH(CONFIG_VOLTAGE_PIN), ADC_ATTEN_DB_11));

        esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &calib_info);

    }
        if (err == ESP_OK) adcinit = true;
        return err;
}

// only using adc1 (adc2 unavailable, since we use wifi)
uint32_t get_voltage(void)
{
    if (!adcinit) init_adc();

    // Take a test measurement to see if an external high supply voltage is even present 
    uint32_t voltage = 0;

    uint8_t i;
    for (i = 0; i < 100; i++) voltage += adc1_get_raw(ADC_CH(CONFIG_VOLTAGE_PIN));

    voltage /= i;
    voltage = esp_adc_cal_raw_to_voltage(voltage, &calib_info) * CONFIG_VOLTAGE_DIV;

    return voltage;
}

esp_err_t send_measure(measureptr item)
{
    ESP_ERROR_CHECK(init_mqtt());
    if (!connected) ESP_ERROR_CHECK(set_conn(true));
    ESP_LOGI(TAG, "Sending measurement.");
    char topic[25];
    sprintf(topic, "/sensor/ESP_%02X%02X%02X/mes", item->device_id[3], item->device_id[4], item->device_id[5]);
    // 2nd string for message?
    // max extra chars: 14 + 4 byte hex timestamp + 6 byte hex id, with 1 decimal for each measurement
    char message[81]; 
    sprintf(message, "{" 
                        "\"id\":\"%02x%02x%02x%02x%02x%02x\","
                        "\"timestamp\":\"%lx\","
                        "\"temp\":%.1f,"
                        "\"pres\":%.1f,"
                        "\"humi\":%.1f" 
                     "}", MAC2STR(item->device_id), item->timestamp, (item->temperature / 100.0f), (item->pressure / 10000.0f), (item->humidity / 1024.0f) );
    for (uint8_t try = 0; !connected && (try < 8); try++ )
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    
    
    if (connected)
    {
        int id = esp_mqtt_client_publish(client, topic, message, 0, 0, 0);

        if (id == 0)
        {
            ESP_LOGI(TAG, "Send successful.");
            return ESP_OK;
        }
        else
            return ESP_FAIL;
    }
    else
    {
        ESP_LOGE(TAG, "No connection to server established in time. Returning...");
        return ESP_ERR_TIMEOUT;
    }
}

esp_err_t send_status(statusmsg *status) // MAC for writing id
{
    ESP_ERROR_CHECK(init_mqtt());
    if (!connected) ESP_ERROR_CHECK(set_conn(true));
    ESP_LOGW(TAG, "Sending status.");
    char topic[27];
    sprintf(topic, "/sensor/ESP_%02X%02X%02X/status", status->mac[3], status->mac[4], status->mac[5]);
    // Get time, voltage and chip temp, put in JSON string
    //
    wifi_ap_record_t ap_info;
    ESP_ERROR_CHECK(esp_wifi_sta_get_ap_info(&ap_info)); 
    status->chiptemp = ( (temprature_sens_read() - 32) / 1.8); // temp is in Fahrenheit
    status->voltage = get_voltage();
    status->rssi = ap_info.rssi;
    status->timestamp = time(NULL);

    char message[100];
    sprintf(message, "{"
                        "\"id\":\"%02x%02x%02x%02x%02x%02x\","
                        "\"timestamp\":\"%lx\","
                        "\"chiptemp\":%.1f,"
                        "\"vcc\":%d,"
                        "\"rssi\":%d"
                        /* "\"name\":\"%s\"" */
                     "}", MAC2STR(status->mac), status->timestamp, status->chiptemp, status->voltage, status->rssi);
    
    for (uint8_t try = 0; !connected && (try < 5); try++ )
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    
    
    if (connected)
    {
        int id = esp_mqtt_client_publish(client, topic, message, 0, 1, 0);
        if (id != (-1))
        {
            ESP_LOGI(TAG, "Send of status successful.");
            return ESP_OK;
        }
        else
            return ESP_FAIL;
    }
    else 
    {
        ESP_LOGE(TAG, "No connection to server established in time. Returning...");
        return ESP_ERR_TIMEOUT;
    }
}

esp_err_t send_fullstat(statusmsg *status) // MAC for writing id
{
    ESP_ERROR_CHECK(init_mqtt());
    if (!connected) ESP_ERROR_CHECK(set_conn(true));
    ESP_LOGW(TAG, "Sending full status.");
    char topic[27];
    sprintf(topic, "/sensor/ESP_%02X%02X%02X/status", status->mac[3], status->mac[4], status->mac[5]);
    // Get time, voltage and chip temp, put in JSON string
    //
    wifi_ap_record_t ap_info;
    ESP_ERROR_CHECK(esp_wifi_sta_get_ap_info(&ap_info));
    status->chiptemp = ( (temprature_sens_read() - 32) / 1.8); // temp is in Fahrenheit
    status->voltage = get_voltage();
    status->rssi = ap_info.rssi;
    status->timestamp = time(NULL);
    char message[160];

    sprintf(message, "{"
                        "\"id\":\"%02x%02x%02x%02x%02x%02x\","
                        "\"timestamp\":\"%lx\","
                        "\"lastreset\":\"%s\","
                        "\"chiptemp\":%.1f,"
                        "\"vcc\":%d,"
                        "\"rssi\":%d,"
                        "\"name\":\"%s\""
                     "}", MAC2STR(status->mac), status->timestamp, status->lastreset, status->chiptemp, status->voltage, status->rssi,status->name);
    
    for (uint8_t try = 0; !connected && (try < 5); try++ )
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    
    
    if (connected)
    {
        int id = esp_mqtt_client_publish(client, topic, message, 0, 1, 0);
        if (id != (-1))
        {
            ESP_LOGI(TAG, "Send of status successful.");
            return ESP_OK;
        }
        else
            return ESP_FAIL;
    }
    else
    {
        ESP_LOGE(TAG, "No connection to server established in time. Returning...");
        return ESP_ERR_TIMEOUT;
    }
}

esp_err_t init_config_listen(u8_t *id)
{
    char listen[32];
    sprintf(listen, "/sensor/ESP_%02X%02X%02X/config", id[3], id[4], id[5]);

    ESP_ERROR_CHECK(init_mqtt());
    if (!connected) ESP_ERROR_CHECK(set_conn(true));
    ESP_LOGD(TAG, "Subscribing to config channel.");
    if (connected)
    {    
        int id = esp_mqtt_client_subscribe(client, listen, 2);
        if (id != (-1))
        {
            ESP_LOGI(TAG, "Config subscription successful.");
            return ESP_OK;
        }
        else
            return ESP_FAIL;
    }
    else
    {
        ESP_LOGE(TAG, "No connection to server established in time. Returning...");
        return ESP_ERR_TIMEOUT;
    }
}

esp_err_t send_config(u8_t ctime, u16_t stime, char *name, u8_t *id)
{
    char listen[32];
    sprintf(listen, "/sensor/ESP_%02X%02X%02X/config", id[3], id[4], id[5]);

    ESP_ERROR_CHECK(init_mqtt());
    if (!connected) ESP_ERROR_CHECK(set_conn(true));
    ESP_LOGW(TAG, "Sending config.");

    char message[75];
    sprintf(message, "{"
                        "\"id\":\"%02x%02x%02x%02x%02x%02x\","
                        "\"CTIME\":%d,"
                        "\"STIME\":%d,"
                        "\"name\": \"%s\""
                    "}", MAC2STR(id), ctime, stime, name
            );

    if (connected)
    {
        int id = esp_mqtt_client_publish(client, listen, message, 0, 1, 0);
        if (id != (-1))
        {
            ESP_LOGI(TAG, "Send of config successful.");
            return ESP_OK;
        }
        else
            return ESP_FAIL;
    }
    else
    {
        ESP_LOGE(TAG, "No connection to server established in time. Returning...");
        return ESP_ERR_TIMEOUT;
    }
}

static void recv_config(char *config_str)
{
    xTaskNotify(mqtt_conf_handle, (u32_t)config_str, eSetValueWithOverwrite); // To ensure config will be set only once before restarting
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    /* esp_mqtt_client_handle_t client = event->client;
    int msg_id; */
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGW(TAG, "MQTT_EVENT_CONNECTED");
            connected = true;
            xTaskNotify(mqtt_handle, 2, eSetValueWithOverwrite);
            xTaskNotify(mqtt_conf_handle, 0, eSetValueWithoutOverwrite);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGE(TAG, "MQTT_EVENT_DISCONNECTED");
            connected = false;
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            if ( strstr(event->topic, "config") ) { // right now, the device's config channel is the only subscribed channel anyway, but still...
                recv_config(event->data);
            } else {
                ESP_LOGI(TAG, "TOPIC=%.*s\r\n", event->topic_len, event->topic);
                ESP_LOGI(TAG, "DATA=%.*s\r\n", event->data_len, event->data);
            }
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGE(TAG, "MQTT_EVENT_ERROR");
            connected = false;
            break;
        case MQTT_EVENT_BEFORE_CONNECT:
            ESP_LOGW(TAG, "MQTT_EVENT_BEFORE_CONNECT");
            ESP_LOGW(TAG, "Connecting to: " CONFIG_MQTT_URI );
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

void stop_mqtt(void)
{
    esp_mqtt_client_stop(client);
}
