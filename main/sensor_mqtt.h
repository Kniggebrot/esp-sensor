#ifndef SENSOR_MQTT_H
#define SENSOR_MQTT_H

#include "esp_err.h"
#include <stdint.h>

#include "BME280.h"

#undef MACSTR
#define MACSTR "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx"
#define MAC2SSTR(a) &(a)[0], &(a)[1], &(a)[2], &(a)[3], &(a)[4], &(a)[5]
/* Setting targets for scanf-function-family */

extern TaskHandle_t mqtt_handle;
extern TaskHandle_t mqtt_conf_handle;

typedef struct
{
    uint8_t mac[6];
    char name[9];
    time_t timestamp;

    uint32_t voltage;
    float chiptemp;
    char lastreset[19];
    int8_t rssi;
} statusmsg;

esp_err_t send_measure(measureptr item);
esp_err_t send_status(statusmsg *status);
esp_err_t send_fullstat(statusmsg *status);
esp_err_t init_mqtt(void);
esp_err_t init_config_listen(u8_t *id);
esp_err_t send_config(u8_t ctime, u16_t stime, char *name, u8_t *id);

#endif // SENSOR_MQTT_H
