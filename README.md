# ESP32: Weather sensor

Using a temperature and humidity sensor ([BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/)), the ESP will log the current temperature and humidity and procede to send it to a Raspberry Pi for logging.

This project uses Larry Bank's (bitbank2) font bytearray from his oled_96 project as font.c in the SSD1306 module. The character drawing functions were adapted from it as well, 
the complete project can be found on [Github](https://www.github.com/bitbank2/oled_96).